#!/bin/bash

sudo rm -f /var/lib/dpkg/info/ros-melodic*
sudo rm -f /var/lib/dpkg/info/python-ros*

sudo apt --fix-broken install

cd ~/catkin_ws && catkin_make

source ~/.bashrc

sudo apt install python-rosdep2
sudo rosdep init
rosdep update
