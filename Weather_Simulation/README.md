# Weather Simulation

## Possible weather parameters
Temperature | Humidity | Atmospheric Pressure and Density | Wind | Clouds | Precipitation | Evaporation | Atmospheric Stability | Snow and Frost | Radiation

According to a broad research, there is not official or reliable plugin that could simulate the weather from all aspects. And under the consideration that the drone wouldn't be used in extreme weather conditions, the major factor we will consider in our simulation will be wind. So we basically assume that the drone would't face rain or snow. Then wind is the only factor we will consider that may affect the process of delivery.

Based on conditions above, we will use SITL from ArduPilot to test the effects of wind.

## SITL parameters
To see other wind parameters do:

`param show sim_wind*`

There are four essential parameters:

`SIM_WIND_DIR`

SIM_WIND_DIR sets the direction of the wind. Possible values are from 0 to 360, and the unit will be degrees.

`SIM_WIND_SPD` 

SIM_WIND_SPD sets the speed of the wind in meters per second.

`SIM_WIND_TURB`

SIM_WIND_TURB randomly varies the horizontal and vertical wind speed. After recreating the code in MATLAB and testing, it seems that every time step the code adds vector to the wind at a random direction. In each direction (Horizontal and Vertical), the mean deviation from 0 is 0.16*SIM_WIND_TURB. For example, setting SIM_WIND_TURB to 10 would result in both horizontal (in a random yaw) and vertical deviations in the windspeed vector, with a mean deviation of 1.6 m/s.

`SIM_WIND_DELAY` 

SIM_WIND_DELAY delays the sending of windspeed information, simulating part of an airspeed sensor. This is measured in milliseconds (ms). This is described in comments in the code.


Sample code:

`param set SIM_WIND_DIR 180`

`param set SIM_WIND_SPD 10`

## Recommand parameter for simulation

### Direction

According to research on Sydney's weather, wind direction is not directly related wind speed, as both of them are affected by seasons actually. So we may use four basic directions for simulation, that is North, East, South, West.

Parameters will be 0, 90, 180, 270 respectively.

### Speed

According to Beaufort Wind Scale, wind that is faster than 10 m/s will make drone flight difficult, so our simulation would not exceed that.

Then, according to the wind scale 0 - 5, we take the average wind speed in each scale.

| Scale | Speed |
| -- | -- |
| 0 | 0 |
| 1 | 1 |
| 2 | 2.5 |
| 3 | 4.25 |
| 4 | 6.75 |
| 5 | 9.5 |

## Useful Link

Tutorial on how to use SITL.

https://ardupilot.org/dev/docs/using-sitl-for-ardupilot-testing.html?highlight=wind

An essay about effects of wind, with sample code of flight using ArduPilot.

https://repository.asu.edu/attachments/135075/content/Biradar_asu_0010N_13909.pdf

Explanation for TURB and DELAY.

https://diydrones.com/forum/topics/testing-the-effects-of-wind-with-sitl

Weather parameters.

https://www.meteomatics.com/en/api/available-parameters/basic-weather-parameter/

Wind speed scale.

http://gyre.umeoce.maine.edu/data/gomoos/buoy/php/variable_description.php?variable=wind_2_speed

Sydney Weather.

https://weatherspark.com/y/144544/Average-Weather-in-Sydney-Australia-Year-Round