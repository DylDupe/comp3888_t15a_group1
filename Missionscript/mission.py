import time, sys, math, argparse
from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
from pymavlink import mavutil
import json


parser = argparse.ArgumentParser()

parser.add_argument("--px4", action="store_true")
parser.add_argument("--ardu", "--ardupilot", action="store_true")
parser.add_argument("-f", "--file", type=str, default="square_mission.json", help="the mission coordinates file")
parser.add_argument("-x", "--xoffset", type=float, default=1, help="the x start coordinate of the drone")
parser.add_argument("-y", "--yoffset", type=float, default=1, help="the y start coordinate of the drone")
parser.add_argument("-p", "--port", type=int, default="14540", help="port to connect to the flight board. PX4 port = 14540, Ardupilot port = 5762.")

args = parser.parse_args()

# The altitude that the drone will fly to for each takeoff. Should be greater than the height of all waypoints.
DEFAULT_ALT = 20


"""
Connects to a vehicle/drone
RET: vehicle object (DroneKit)
"""
def connect_vehicle():
    
    # connect to different port based on if its PX4 or Ardupilot board
    if args.ardu:
        connection_string = "tcp:127.0.0.1:5762"
    else:
        connection_string = '127.0.0.1:' + str(args.port)


    print("Connecting to drone on " + connection_string)
    vehicle = connect(connection_string, wait_ready=True)

    print(" Type: ", vehicle._vehicle_type)
    print(" Armed: ", vehicle.armed)
    print(" System status: ",  vehicle.system_status.state)
    print(" GPS: ", vehicle.gps_0)
    print(" Alt: ", vehicle.location.global_relative_frame.alt)

    return vehicle


"""
Runs the mission on a PX4 drone
PARAM: wp_list - a list of waypoints loaded from a file
"""
def px4_mission(wp_list):
    # home_position_set = False

    # wait for a home position lock
    # while not home_position_set:
    #     print("Waiting for home position...")
        
    #     time.sleep(1)

    # Set PX4 autopilot to AUTO mode. 
    vehicle._master.mav.command_long_send(vehicle._master.target_system, vehicle._master.target_component,
                                               mavutil.mavlink.MAV_CMD_DO_SET_MODE, 0,
                                               4,
                                               0, 0, 0, 0, 0, 0)

    for wp in wp_list:
        cmds.add(wp)

    cmds.upload()

    time.sleep(2)

    vehicle.armed = True

    nextwaypoint = vehicle.commands.next
    waypoint_ctr = 1
    section_ctr = 1
    print("Taking off")


    # Displays the waypoint during the mission
    while nextwaypoint < len(vehicle.commands):
        display_current_pos_offset(vehicle)    
        if vehicle.commands.next > nextwaypoint:
            display_seq = vehicle.commands.next+1

            if section_ctr == 3 or display_seq == 2:
                print("Moving to waypoint", waypoint_ctr)
                waypoint_ctr += 1
                section_ctr = 0

            section_ctr += 1            
            nextwaypoint = vehicle.commands.next
        time.sleep(1)
        

    # Wait for the vehicle to finish mission
    while vehicle.commands.next > 0:
        time.sleep(1)



"""
Run the mission on an Ardupilot drone. Currently not working
PARAM: wp_list - a list of waypoints loaded from a file
"""
def ardu_mission(wp_list):

    
    while not vehicle.is_armable:
        print(" Waiting for vehicle to initialise...")
        time.sleep(1)

        
    print("Arming motors")

    # Copter should arm in GUIDED mode
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    while not vehicle.armed:      
        print(" Waiting for arming...")
        time.sleep(1)

    
    vehicle.commands.next=0

    for wp in wp_list:
        cmds.add(wp)

    cmds.upload()

    # MAVLink Mission_Start message, required for an Ardupilot Mission to start
    msg = vehicle.message_factory.command_long_encode(
        0, 0,
        mavutil.mavlink.MAV_CMD_MISSION_START,
        0,
        0,
        1,
        0,
        0,0,0,0
    )

    vehicle.send_mavlink(msg)

    time.sleep(2)

    
    vehicle.mode = VehicleMode("AUTO")

    # Displays the waypoint during the mission
    nextwaypoint = vehicle.commands.next
    while nextwaypoint < len(vehicle.commands):
        display_current_pos_offset(vehicle)
        if vehicle.commands.next > nextwaypoint:
            display_seq = vehicle.commands.next+1
            print("Moving to waypoint %s" % display_seq)
            nextwaypoint = vehicle.commands.next
        time.sleep(1)
        

    # Wait for the vehicle to finish the mission
    while vehicle.commands.next > 1:
        time.sleep(1)


"""
use this function to calculate the waypoint location
RET: LocationGlobal object specifying an absolute waypoint coordinate
"""
def get_location_offset_meters(original_location, dNorth, dEast, alt):
    """
    Returns a LocationGlobal object containing the latitude/longitude `dNorth` and `dEast` metres from the
    specified `original_location`. The returned Location adds the entered `alt` value to the altitude of the `original_location`.
    The function is useful when you want to move the vehicle around specifying locations relative to
    the current vehicle position.
    The algorithm is relatively accurate over small distances (10m within 1km) except close to the poles.
    For more information see:
    http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    """
    earth_radius=6378137.0 #Radius of "spherical" earth
    #Coordinate offsets in radians
    dLat = dNorth/earth_radius
    dLon = dEast/(earth_radius*math.cos(math.pi*original_location.lat/180))

    #New position in decimal degrees
    newlat = original_location.lat + (dLat * 180/math.pi)
    newlon = original_location.lon + (dLon * 180/math.pi)
    return LocationGlobal(newlat, newlon,original_location.alt+alt)

"""
obtains the location of an waypoint by applying the starting location as an offset to the waypoints, such that it is relative to the drone's location
"""
def get_waypoint(coordinates):
    return get_location_offset_meters(home, coordinates[0] - args.yoffset, coordinates[1] - args.xoffset, coordinates[2])


"""
Load the mission waypoints from a file. Different for PX4 and Ardupilot
PARAM: json_file - the JSON file object
       ardupilot - whether the missino is run on an ardupilot board. Default is False.
RET: list of waypoints
"""
def load_mission_waypoints(json_file, ardupilot=False):

    output_dict = json.load(json_file)

    first_wp = True
    wp_list = []

    # adding dummy mission wp for ardupilot because it ignores the first mission cmd
    if ardupilot:
        cmd = takeoff_command(get_waypoint([0, 0, 0]))
        wp_list.append(cmd)

    # load each waypoint from the config file
    waypoint_counter = 1
    
    for waypoint in output_dict["path"]:
        wp = get_waypoint(waypoint)

        # land for good if it is the last waypoint
        if (waypoint_counter == len(output_dict["path"])):
            wp = get_waypoint([args.yoffset, args.xoffset, 0])
            cmd = waypoint_command(wp)
            wp_list.append(cmd)
            cmd = land_command(wp)
            wp_list.append(cmd)

        else: # all other waypoints
            # First waypoint will be a takeoff command
            if first_wp:
                cmd = takeoff_command(wp)
                wp_list.append(cmd)
                first_wp = False
            else:
                # all other waypoints will go to the waypoint, land and takeoff again
                cmd = waypoint_command(wp)
                wp_list.append(cmd)
                cmd = land_command(wp)
                wp_list.append(cmd)
                cmd = takeoff_command(wp)
                wp_list.append(cmd)
        
        waypoint_counter += 1

    return wp_list




"""
Functions that abstract the MAVLink commands
PARAM: wp - waypoint object
RET: a Command object (DroneKit) containing the MAVLink Command
"""
def takeoff_command(wp):
    return Command(0,0,0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_TAKEOFF, 0, 1, 0, 0, 0, 0, wp.lat, wp.lon, DEFAULT_ALT)

def waypoint_command(wp):
    return Command(0,0,0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 1, 0, 0, 0, 0, wp.lat, wp.lon, DEFAULT_ALT)

def land_command(wp):
    return Command(0,0,0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_LAND, 0, 1, 0, 0, 0, 0, wp.lat, wp.lon, wp.alt)

# sends a return command via mavlink
def return_command():
    vehicle._master.mav.command_long_send(1, 1, mavutil.mavlink.MAV_CMD_NAV_LAND, 0, 0, 0, 0, 0, home.lat, home.lon, home.alt)


"""
Prints current location of the drone
"""
def display_current_pos_offset(vehicle):
    vehicle_pos = vehicle.location.local_frame
    print("Lon: {:.2f}, Lat: {:.2f}, Alt: {:.2f}".format(vehicle_pos.north, vehicle_pos.east, -1*vehicle_pos.down))
    

# ------------main---------------#

# 
try:
    mission_file = open(args.file, )
except FileNotFoundError:
    sys.stderr.write("Mission file not found: {}\nAbort mission\n".format(args.file))
    sys.exit(1)


# vehicle is a global variable should change this (maybe)
vehicle = connect_vehicle()

# Load commands
cmds = vehicle.commands
cmds.clear()

# Set the home location of the vehicle
home = vehicle.location.global_relative_frame

# Run the mission differently for PX4 and Ardupilot
if args.ardu:
    wp_list = load_mission_waypoints(mission_file, ardupilot=True)
    ardu_mission(wp_list)
else:
    vehicle.parameters["SIM_BAT_MIN_PCT"] = 0
    wp_list = load_mission_waypoints(mission_file)
    px4_mission(wp_list)
    

print("Finishing Mission")


# Close vehicle object before exiting script
vehicle.close()
time.sleep(1)