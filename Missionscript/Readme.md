# Mission Script
```
Usage: python3 mission.py [-f] [mission_file] <optional> [--px4|--ardu] [-x|--xoffset starting_x] [-y|--yoffset starting_y] [-p|--port port_number]
```

Uploads and runs a mission on the drone.

Flags:

[-f] : Specify the JSON file to load waypoints from. Default is square_mission.json.

\<optional>


[--px4|--ardu] : Specify the flight stack. Default is PX4.

[-x|--xoffset starting_x] : The starting x coordinates of the drone. Default is 1 as it is where PX4 spawns the drone in Gazebo by default. See Drone Control Documentation for how to specify starting location with ROS. 

[-y|--yoffset starting_y] : The starting y coordinate of the drone. Default is 1 as it is where PX4 spawns the drone in Gazebo by default.

[-p|--port port_number] : The port to connect to the drone. Default is 14540 for PX4. Use this for multi-drone mission to connect to different drones, preferably specifying the -x -y flags as each drone start at a different location. 


## Example Usage
Runs a mission where the drone goes in a square pattern around the home location
```
python3 mission.py -f square_mission.json --px4
```

## Small Things
The connection to the vehicle can be buggy sometimes, especially if the mission script is launched right after Gazebo started. Sometimes it requires multiple execution of the mission.py in order to get the drone flying properly.


## PX4
The script connects to the PX4 board through the address 127.0.0.1:14540

## Ardupilot
**This program currently does not work with ArduPilot**

There are more work needed in order to get it working with ArduPilot. There's a lot of differences between PX4 and ArduPilot in order of their message protocols. Generally, MavLink messages are the same between the two, while DroneKit is more designed for ArduPilot.

The script connects to the Ardupilot board through the address tcp:127.0.0.1:5762

The Ardupilot script contains a bit more nuance, with an extra MAV_CMD_MISSION_START message required to start the message and it also ignores the first mission waypoint, so a dummy waypoint needs to be prepended to the mission.

Ardupilot initialisation takes longer, so wait a little more for everything to load before executing the script.

## Waypoints
Currently the waypoints specified by the file is taken as relative to the drone's position. May need to change this implementation depending on how development goes and coordinate (lol) with the algorithm team.

Waypoints are being loaded from a json file specified by the -f flag and is looking for the field "path" as the waypoints for the mission.

Waypoint vectors are in the form of [east, north, altitude]
A waypoint vector of [-10, -10, 10] meanings to go west 10 metres and go south 10 metres at an altitude of 10 metres.

## Mission Files
Currently only reading the "path" section of the json file

Sample Mission file - Square Mission
```
{
    ...,
    
    "path":[
        [
            0,
            0,
            10
        ],
        [
            10,
            10,
            10
        ],
        [
            -10,
            10,
            10
        ],
        [
            -10,
            -10,
            10
        ],
        [
            10,
            -10,
            10
        ]
    ],
}
```
## Charging Stations
Charging stations are being considered as normal waypoints as the time penalty from pausing on charging station would be too long to simulate. See the feature branch feature/new_mission_script for an implementation that allows for pausing on a landing pad. 

## Multi-Drone Mission
Multi-Drone Mission can be conducted by connecting to the vehicle on different ports.


First you need to setup Multi-Drone Simulation as specified in the Drone Control Documentation, then launch Gazebo with either


(Without ROS)
```
cd Firmware
Tools/gazebo_sitl_multiple_run.sh [-m <model>] [-n <number_of_vehicles>]
```


(With ROS)
```
roslaunch px4 multi_uav_mavros_sitl.launch
```

and then launch the Mission Script with
```
python3 mission.py -f square_mission.py --px4 -p 14540
```

To connect to the second drone, do in a different terminal
```
python3 mission.py -f square_mission.py --px4 -p 14541
```

The port number starts at 14540 (for PX4) and simply add 1 to the port in order to connect to the next vehicle. Ardupilot ports start at 5762.

Try to specify the starting location of the drones when running the script for a more accurate mission result. This is done by passing the -x \<starting x> -y \<starting y> flags.
```
python3 mission.py -f square_mission.py --px4 -p 14540 -x 1 -y 1
```