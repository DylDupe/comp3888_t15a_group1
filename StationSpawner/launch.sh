#!/bin/bash

# Written by Dylan Duplessis
# Used by spawn.py to open Gazebo with the specified world file
# spawning the drone at the latitude and longitude passed to spawn.py

if [ -d $FIRMWARE/launch ]; then
  cd $FIRMWARE/launch
  if [ -f posix_sitl.launch ]; then
    #sed -i -e '6 s/0/$1/' -e '7 s/0/$2' $FIRMWARE/posix_sitl.launch
    roslaunch posix_sitl.launch x:=$1 y:=$2
  fi
fi
