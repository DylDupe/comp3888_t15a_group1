import unittest
from spawn import *

class SpawnerTests(unittest.TestCase):

    def test_validate_world_true(self):
        self.assertTrue(validate_world(False, "test_world.world"))

    def test_validate_world_exception_ending(self):
        with self.assertRaises(InvalidWorldFile):
            validate_world(False, "test_world")

    def test_validate_world_exception_existance(self):
        with self.assertRaises(InvalidWorldFile):
            validate_world(False, "fake_world.world")

    def test_validate_world_exception_unloaded(self):
        with self.assertRaises(InvalidWorldFile):
            validate_world(False, "empty.world")

    def test_check_constaints_too_large(self):
        with self.assertRaises(SystemExit) as cc:
            check_constraints(10,9)
        self.assertEqual(cc.exception.code, -1)

    def test_check_constaints_insufficient_distance(self):
        with self.assertRaises(SystemExit) as cc:
            check_constraints(5,5)
        self.assertEqual(cc.exception.code, -1)

    def test_within_barrier_false(self):
        self.assertFalse(within_barrier((10,10), 5))

    def test_within_barrier_true(self):
        self.assertTrue(within_barrier((2,2), 5))

    def test_within_barrier_on_barrier(self):
        self.assertFalse(within_barrier((5,0), 5))

    def test_randomise_point(self):
        pass

if __name__ == '__main__':
    unittest.main()
