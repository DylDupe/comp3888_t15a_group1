import os, sys, configparser
from pathlib import Path

"""
This program installs the python3 dependencies required to run the program spawn.py.
It also defines a config file named 'default.ini' and sets default variables used by spawn.py
including a path to the folder containing Gazebo world files.
"""

if (len(sys.argv) == 2):
  if (sys.argv[1] == "-v"):
    world_path = input("Please enter the path Gazebo stores it's world files: ")
  else:
    print("INVALID USE")
    sys.exit(-1)
elif (len(sys.argv) == 1):
  world_path = input()
else:
    print("INVALID USE")
    sys.exit(-1)

if not os.path.exists(world_path):
  print("The path you entered does not exist.")
  sys.exit(-1)

config = configparser.ConfigParser()
config.add_section("default")
config["default"]["world_path"] = world_path
config["default"]["waypoints_file"] = "data/waypoints.txt"
config["default"]["max_stations"] = "5"
config["default"]["max_radius"] = "100"
config["default"]["spawn_barrier"] = "10"
config["default"]["min_station_distance"] = "5"

with open("default.ini", "w") as configfile:
    config.write(configfile)

if not os.path.isfile("data/waypoints.txt"):
    Path('data/waypoints.txt').touch()

# Installing spawner dependencies
os.execv("/bin/bash", ("/bin/bash", "-c", "python3 -m pip install termcolor random-geometry-points"))
