#!/usr/bin/python3

"""
Program to procedurally generate charging stations and way-points within a given Gazebo world file.
"""

import argparse, time, sys, os, re, random, math, configparser, json
import xml.etree.ElementTree as ET
from termcolor import colored
from random_geometry_points.circle2d import Circle2D

banner = """
  █████████
 ███░░░░░███
░███    ░░░  ████████   ██████   █████ ███ █████ ████████    ██████  ████████
░░█████████ ░░███░░███ ░░░░░███ ░░███ ░███░░███ ░░███░░███  ███░░███░░███░░███
 ░░░░░░░░███ ░███ ░███  ███████  ░███ ░███ ░███  ░███ ░███ ░███████  ░███ ░░░
 ███    ░███ ░███ ░███ ███░░███  ░░███████████   ░███ ░███ ░███░░░   ░███
░░█████████  ░███████ ░░████████  ░░████░████    ████ █████░░██████  █████
 ░░░░░░░░░   ░███░░░   ░░░░░░░░    ░░░░ ░░░░    ░░░░ ░░░░░  ░░░░░░  ░░░░░
             ░███
             █████
            ░░░░░

Welcome.
"""

##############################
# Configuration File Parsing #
##############################

config = configparser.ConfigParser()
config.read('default.ini')

###########
# Globals # ----------------------------------------------------------------------------------------------------------------------------------
###########

MAX_STATIONS = int(config["default"]["max_stations"])
MAX_RADIUS = int(config["default"]["max_radius"])
SPAWN_BARRIER = int(config["default"]["spawn_barrier"])
MIN_STATION_DISTANCE = int(config["default"]["min_station_distance"])
DEFAULT_WORLD_PATH = config["default"]["world_path"]
DEFAULT_WAYPOINTS_FILE = config["default"]["waypoints_file"]
if not DEFAULT_WORLD_PATH.endswith("/"):
    DEFAULT_WORLD_PATH = DEFAULT_WORLD_PATH + "/"
# TODO: check for environment variable existence
try:
    static_path = os.environ['SOURCE'] + "/StationSpawner/data/"
except KeyError:
    print("Please make sure you have the environment variable SOURCE set. Check install.sh or setup.py for further information.")
    sys.exit(1)

###########
# Parsing # ----------------------------------------------------------------------------------------------------------------------------------
###########

def parse_args(args):
    description = ""
    parser = argparse.ArgumentParser(description = description)
    parser.add_argument("lat", type=float, help="Latitude of the drone")
    parser.add_argument("long", type=float, help="Longitude of the drone")
    parser.add_argument("world", type=str, help="Name of world file")
    parser.add_argument('-n', "--number", type=int, default=MAX_STATIONS, help='Number of charging stations to spawn')
    parser.add_argument('-r', '--radius', type=int, default=MAX_RADIUS, help='Radius to spawn within') # TODO: Restrict values using 'choices=[]'
    parser.add_argument('-d', '--distance', type=int, default=MIN_STATION_DISTANCE, help='Set the minimum distance between stations')
    parser.add_argument('-b', '--barrier', type=int, default=SPAWN_BARRIER, help='Minimum distance between drone and stations')
    command_group = parser.add_mutually_exclusive_group()
    command_group.add_argument('-w', '--waypoints', action = "store_true", help='File containing waypoints to generate')
    command_group.add_argument('-s', '--static', type=str, help='File containing charging station and waypoint coordinates to spawn')
    parser.add_argument('-m', '--map', action = "store_true", help='Show the map for generated stations')
    parser.add_argument('-v', '--verbose', action = "store_true", help='Display banner and ask to confirm parameters')
    parser.add_argument('-c', '--custom', action = "store_true", help='Use custom path for world file')
    return parser.parse_args(args)

############################
# Custom Exception Classes #
############################

class InvalidFile(Exception):
    """Exception raised for errors in the specified world file.

    Attributes:
        filename -- name of the world file
        type -- type of world error
    """

    def __init__(self, filename, type):
        self.filename = filename
        self.world = filename.split("/")[-1]
        self.type = type
        if type == "ending":
            self.message = f"Valid world file \"{self.world}\" must end with .world"
        elif type == "existance":
            self.message = f"File at \"{filename}\" does not exist"
        # elif type == "unloaded":
        #     self.message = f"World file \"{filename}\" must have already been loaded and saved by Gazebo, see https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/StationSpawner/ for further explanation"
        super().__init__(self.message)

class ConstraintViolation(Exception):
    """Exception raised for violated constraints as per the check_constraints function.

    Attributes:
        name -- name of constraint violated
    """

    def __init__(self, name, filename = None):
        self.name = name
        if name == "barrier":
            self.message = "Barrier is too large. Please check default.ini or parse new parameters"
        elif name == "distance":
            self.message = f"There is not enough distance between the barrier and the radius. Please check default.ini or parse new parameters"
        super().__init__(self.message)

#####################
# Passive Functions # ------------------------------------------------------------------------------------------------------------------------
#####################

def pprint(text, value):
    print(colored(">> ", "white"), text ,": ", colored(value, "cyan"))

####################
# Helper functions # -------------------------------------------------------------------------------------------------------------------------
####################

def check_constraints(args):
    """
    Checks some base constraints which may not be violated.

    @param radius: Radius as set in default.ini (the maximum radius) or by the user via the -r flag.
    @type radius: int (metres)
    @param barrier: Minimum distance the stations must be away from the drone
    @type barrier: int
    @param waypoints_file:
    @type waypoints_file: str
    """
    if args.barrier >= args.radius:
        raise ConstraintViolation("barrier")
    elif (args.radius - args.barrier) < 5:
        raise ConstraintViolation("distance")
    if args.static and args.waypoints:
        raise ConstraintViolation("arguments")

def validate_world(custom, world_file):
    """
    Checks the validity of the specific world file

    @param custom: flag which determines if the specified world file is
                   within the default path as in default.ini
    @type custom: boolean
    @param world: path of valid world file
    @type world: str
    @return world_file: path of valid world file
    @rtype: str
    @raises InvalidWorldFile: if there is an error with the world file
    """
    if custom:
        # if the -c flag is given, world_file is actually a path
        if not world_file.split("/")[-1].endswith(".world"):
            raise InvalidFile(world_file, "ending")
    else:
        # world_file is just the name of a world file
        if not DEFAULT_WORLD_PATH.endswith("/"): # Here we redefine world_file to be a path using the default path as in default.ini
            world_file = DEFAULT_WORLD_PATH + "/" + world_file
        else:
            world_file = DEFAULT_WORLD_PATH + world_file

    if not world_file.endswith(".world"):
        raise InvalidFile(world_file, "ending")

    if not os.path.exists(world_file):
        raise InvalidFile(world_file, "existance")

    # with open(world_file, "r") as f:
    #     if any([re.search("include", x) for x in f.readlines()]):
    #         raise InvalidFile(world_file.split("/")[-1], "unloaded")

    return world_file

def confirm_params(number, radius, barrier):
    print(banner)
    pprint("Number of Stations", number)
    pprint("Radius", radius)
    pprint("Barrier", barrier)
    print()
    user_input = input(colored("Confirm these parameters? Y/N ", "white"))
    if user_input.lower() not in ["n", "no"]:
        return True
    print("Goodbye")
    return False

def read_static_objects(filename):
    with open (static_path + filename, "r") as f:
        lines = [x.strip("\n").split(",") for x in f.readlines()]
        number_stations = int(lines[0][0])
        points = [(float(x[0]),float(x[1])) for x in lines[1:number_stations+1]]
        waypoints = [(float(x[0]),float(x[1])) for x in lines[number_stations+1:]]

    return points, waypoints, max([extend_radius(points), extend_radius(waypoints)])

def within_barrier(point, barrier):
    x_distance = abs(point[0])
    y_distance = abs(point[1])
    if x_distance < barrier and y_distance < barrier:
        return True
    else:
        return False

def randomise_point(circum_point, radius, barrier):
    # In order to generate points IN the circle, we take the point on the circumference
    # and subtract or add randomness depending on the sign of the original x-point, y-point.
    circum_x = circum_point[0][0] # x from [(x, y)]
    circum_y = circum_point[0][1] # y from [(x, y)]
    randomness = random.random() * (radius - barrier)
    if circum_x < 0:
        x = circum_x + randomness
    elif circum_x > 0:
        x = circum_x - randomness
    if circum_y < 0:
        y = circum_y + randomness
    elif circum_y > 0:
        y = circum_y - randomness

    return (x,y)

def random_point(origin, radius, barrier):
    # Create Circle2D object using (x,y) = (origin[0], origin[1]) and radius `radius`
    circle = Circle2D(origin[0], origin[1], radius)
    # Generate a random point ON the circumference of the Circle2D object
    random_circumference_point = circle.create_random_points(1)

    # Taking the point on the circumference and randomising it be somewhere within the circle
    random_circle_point = randomise_point(random_circumference_point, radius, barrier)

    # This ensures that a station isn't spawned too close to the starting location of the drone
    while within_barrier(random_circle_point, barrier):
        random_circumference_point = circle.create_random_points(1)
        random_circle_point = randomise_point(random_circumference_point, radius, barrier)

    return random_circle_point

def valid_point(point, points, waypoints):
    """
    Check whether the newly generated point is within the distance barrier of other generated points.

    @param point: (x,y) coordinate to check validity of
    @type point: tuple
    @param points: list of (x,y) coordinates which have already been deemed valid
    @type points: list
    @param waypoints: list of (x,y) coordinates representing waypoints
    @type waypoints: list
    @return bool: True if new point is far enough away from valid points
    @rtype: bool
    """
    # Loop through already valid points
    for valid_point in points:
        # Calculate distance between new point and all valid points
        distance = math.sqrt(math.pow(point[0] - valid_point[0], 2) + math.pow(point[1] - valid_point[1], 2))
        # Check this distance against the minimum distance
        if distance <= MIN_STATION_DISTANCE:
            return False

    for waypoint in waypoints:
        distance = math.sqrt(math.pow(point[0] - waypoint[0], 2) + math.pow(point[1] - waypoint[1], 2))
        # Check this distance against the minimum distance
        if distance <= 5:
            return False
    return True

def extend_radius(points):
    max_radius = None
    for point in points:
        distance_from_origin = math.sqrt(math.pow(point[0], 2) + math.pow(point[1], 2)) # pythagoras
        if max_radius == None:
            max_radius = distance_from_origin
        else:
            if distance_from_origin > max_radius:
                max_radius = distance_from_origin
    if max_radius == None:
        return MAX_RADIUS
    else:
        return max_radius

def generate_points(num, radius, barrier, waypoints):
    """
    procedurally generate coordinates in a cartesian plane. The first points is within a circle with centre (0,0) and radius of @param radius.
    All points afterwards are generated within a circle with the same radius but varying centres. These new centres are the coordinates of the
    new charging stations and have an equal chance of being picked as the centre for the new point as the (0,0) coordinate.

    @param num: Number of points to generate
    @type num: int
    @param radius: Radius as set in default.ini (the maximum radius) or by the user via the -r flag.
    @type radius: int (metres)
    @param barrier: Minimum distance the stations must be away from the drone
    @type barrier: int
    @param waypoints: list of (x,y) coordinates representing waypoints
    @type waypoints: list
    @return points: list of valid points and extended radius
    @rtype [(x,y)], int
    """
    points = []
    # Generate `num` points to plot
    while len(points) < num:
        if len(points) == 0:
            # Generate a random point WITHIN the circle with radius `radius` and OUTSIDE of the spawn barrier
            point = random_point((0,0), radius, barrier)
        else:
            # This execution path is to ensure that the generation of points is procedural; that is, the spawning area increases as stations are added
            points.append((0,0))
            # This line ensures that a random station is picked (this includes the (0,0) coordinate)
            random_origin = points[random.randint(0, len(points)-1)]
            points.remove((0,0))
            point = random_point(random_origin, radius, barrier)

        # Check new point distance from valid points against the set minimum distance between stations
        if valid_point(point, points, waypoints):
            if point not in points:
                points.append(point)

    extended_radius = extend_radius(points)
    return points, extended_radius

def read_waypoints(waypoints_file):
    waypoints = []
    with open(waypoints_file, "r") as f:
        for i in f.readlines():
            waypoints.append((float(i.split(",")[0]), float(i.split(",")[1].strip("\n"))))
    return waypoints

# def valid_waypoint(point, waypoints):
#     # Loop through already valid points
#     for waypoint in waypoints:
#         # Calculate distance between new point and all valid points
#         distance = math.sqrt(math.pow(point[0] - waypoint[0], 2) + math.pow(point[1] - waypoint[1], 2))
#         # Check this distance against the minimum distance
#         if distance <= 1:
#             return False

def generate_waypoints(radius, barrier):
    num_waypoints = random.randint(3,10)
    waypoints = []
    while len(waypoints) < num_waypoints:
        point = random_point((0,0), radius, barrier)
        # if valid_waypoint(point, waypoints):
        waypoints.append(point)
    return waypoints

def initialise_map():
    map = [[" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21,
           [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21,
           [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21, [" "]*21]
    map[10][10] = "D"
    return map

def plot_points(map, points, waypoints, radius):
    scale = radius/10
    for i, point in enumerate(points):
        x = point[0]
        y = point[1]
        plot_x = math.floor(x / scale)
        plot_y = math.floor(y / scale)
        pprint(f"point {i}", (plot_x, plot_y))
        map[-1*(10+plot_y)][(10+plot_x)] = "S"
    for point in waypoints:
        x = point[0]
        y = point[1]
        plot_x = math.floor(x / scale)
        plot_y = math.floor(y / scale)
        map[-1*(10+plot_y)][(10+plot_x)] = "W"

def print_map(map):
    print("+" + "-"*41 + "+")
    for i in map:
        print("|" + "|".join(i) + "|")
    print("+" + "-"*41 + "+")

def offset_points(points, waypoints, drone_xy):
    drone_x = drone_xy[0] # offset by this amount since px4 spawns the drone at this x offset
    drone_y = drone_xy[1] # offset by this amount since px4 spawns the drone at this y offset
    return [(point[0]+drone_x, point[1]+drone_y) for point in points], [(point[0]+drone_x,point[1]+drone_y) for point in waypoints]

def write_to_input(args, coordinates, waypoints):
    stations = []
    destinations = []
    for point in coordinates:
        stations.append([point[0], point[1], 5])
    for waypoint in waypoints:
        destinations.append([waypoint[0], waypoint[1], 5])
    to_write = {"max_battery"  : 250,
                "start" : [args.lat, args.long, 0],
                "end" : destinations,
                "stations": stations}
    with open("data/input.json", "w+") as json_file:
        json.dump(to_write, json_file)

def remove_previously_saved_world():
    previous_world = DEFAULT_WORLD_PATH + "empty.world"
    if os.path.exists(previous_world):
      os.remove(previous_world)

def create_xml_object_world(num, point, parent, colour):
    if colour == "red":
        model = ET.SubElement(parent, "model", attrib = {"name":f'charging_station_{num}'})
    elif colour == "green":
        model = ET.SubElement(parent, "model", attrib = {"name":f'waypoint_{num}'})
    pose_1 = ET.SubElement(model, "pose", attrib={"frame":''})
    pose_1.text = f"{(point[0]-0.7)} {(point[1]-0.7)} 0 0 -0.0"
    link = ET.SubElement(model, "link", attrib={"name":'link'})
    inertial = ET.SubElement(link, "inertial")
    mass = ET.SubElement(inertial, "mass")
    mass.text = "1"
    inertia = ET.SubElement(inertial, "inertia")
    ixx = ET.SubElement(inertia, "ixx")
    ixx.text = "0.166667"
    ixy = ET.SubElement(inertia, "ixy")
    ixy.text = "0"
    ixz = ET.SubElement(inertia, "ixz")
    ixz.text = "0"
    iyy = ET.SubElement(inertia, "iyy")
    iyy.text = "0.166667"
    iyz = ET.SubElement(inertia, "iyz")
    iyz.text = "0"
    izz = ET.SubElement(inertia, "izz")
    izz.text = "0.166667"
    pose_2 = ET.SubElement(inertial, "pose", attrib={"frame":""})
    pose_2.text = "0 0 0 0 -0 0"
    collision = ET.SubElement(link, "collision", attrib = {"name":'collision'})
    geometry = ET.SubElement(collision, "geometry")
    box = ET.SubElement(geometry, "box")
    size = ET.SubElement(box, "size")
    size.text = "0.5 0.5 0.25"
    max_contacts = ET.SubElement(collision, "max_contacts")
    max_contacts.text = "10"
    surface = ET.SubElement(collision, "surface")
    contact = ET.SubElement(surface, "contact")
    ode_1 = ET.SubElement(contact, "ode")
    bounce = ET.SubElement(surface, "bounce")
    friction = ET.SubElement(surface, "friction")
    torsional = ET.SubElement(friction, "torsional")
    ode_2 = ET.SubElement(torsional, "ode")
    ode_3 = ET.SubElement(friction, "ode")
    visual = ET.SubElement(link, "visual", attrib = {"name":'visual'})
    geometry_2 = ET.SubElement(visual, "geometry")
    box_2 = ET.SubElement(geometry_2, "box")
    size_2 = ET.SubElement(box_2, "size")
    size_2.text = "0.5 0.5 0.25"
    material = ET.SubElement(visual, "material")
    if colour == "red":
        ambient = ET.SubElement(material, "ambient")
        ambient.text = "0.7 0 0 1"
        diffuse = ET.SubElement(material, "diffuse")
        diffuse.text = "0.5 0 0 1"
        emissive = ET.SubElement(material, "emissive")
        emissive.text = "0.5 0 0 1"
    elif colour == "green":
        ambient = ET.SubElement(material, "ambient")
        ambient.text = "0 0.7 0 1"
        diffuse = ET.SubElement(material, "diffuse")
        diffuse.text = "0 0.5 0 1"
        emissive = ET.SubElement(material, "emissive")
        emissive.text = "0 0.5 0 1"
    self_collide = ET.SubElement(link, "self_collide")
    self_collide.text = "0"
    enable_wind = ET.SubElement(link, "enable_wind")
    enable_wind.text = "0"
    kinematic = ET.SubElement(link, "kinematic")
    kinematic.text = "0"

def create_object(num, point, parent, tree, type):
    # Creating the charging stations
    if type == "charger":
        create_xml_object_world(num, point, parent, "red")
    elif type == "waypoint":
        create_xml_object_world(num, point, parent, "green")
    tree.write(DEFAULT_WORLD_PATH + "empty.world")

def parse_xml(world_file):
    tree = ET.parse(world_file)
    root = tree.getroot()
    world_obj = root[0]
    return [tree, world_obj]

def spawn(drone_x, drone_y, coordinates, waypoints, world_file):
    """
    Writes to specified world file, but saving as a new file "LOADME.world", the objects.

    @param coordinates: (x,y) coordinates of charging stations.
    @type coordinates: [(x,y)]
    @param waypoints: (x,y) coordinates of waypoints.
    @type waypoints: [(x,y)]
    @param world_file: string representing the path of the world file
    """
    tree, parent = parse_xml(world_file)
    for i, coordinate in enumerate(coordinates):
        charger = create_object(i, coordinate, parent, tree, "charger")
    for i, coordinate in enumerate(waypoints):
        waypoint = create_object(i, coordinate, parent, tree, "waypoint")

    # Spawn a charging station next to where the drone spawns
    waypoint = create_object("Start", (drone_x, drone_y), parent, tree, "charger")

def launch(args):
    os.execl("/bin/bash", "/bin/bash", "-e", "launch.sh", str(args.lat), str(args.long))



#################
# Main Function # ----------------------------------------------------------------------------------------------------------------------------
#################

def main():
    args = parse_args(sys.argv[1:])
    check_constraints(args)
    world_file = validate_world(args.custom, args.world)
    # Ask the user to confirm the parameters that will be used
    if args.verbose and not confirm_params(args.number, args.radius, args.barrier):
        sys.exit()
    # If the stations and waypoints are BOTH given to spawn
    if args.static:
        points, waypoints, extended_radius = read_static_objects(args.static)
    else:
        # If the waypoints are given
        if args.waypoints:
            waypoints = read_waypoints(DEFAULT_WAYPOINTS_FILE)
        else:
            waypoints = generate_waypoints(args.radius, args.barrier)

        points, extended_radius = generate_points(args.number, args.radius, args.barrier, waypoints)

    if args.map:
        map = initialise_map()
        plot_points(map, points, waypoints, extended_radius)
        print_map(map)

    write_to_input(args, points, waypoints)
    remove_previously_saved_world()
    spawn(args.lat, args.long, points, waypoints, world_file)
    launch(args)

#############
# Main Call # -------------------------------------------------------------------------------------------------------------------------------------
#############

# This is for testing purposes
if (len(sys.argv) == 1):
    pass
else:
    main()
