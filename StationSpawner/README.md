# Spawning Objects in Gazebo #

## Introduction to spawn.py ##

This program procedurally generates charging stations in the following way: given the position of the drone in latitude and longitude and using a maximum radius (unless specified otherwise), charging stations will be spawned within the circle created using this information. Since drones can recharge their battery, the only factor limiting their reach, the generation of each station increases the area of valid spawning points to include area reachable from the new station.

From here, way-points can either be generated randomly using the generated coordinates of the charging station or read from a file. The program outputs a file named `input.json` which is fed into the algorithm to compute the most optimal path. Charging stations are spawned as red neon boxes, whereas the way-points are neon green boxes.

If you ran `install.sh` already, **please disregard the following**. Running `python3 setup.py -v` and entering the path your Gazebo program has stored its world files, this is most often `$(HOME_DIR)/.../Firmware/Tools/sitl_gazebo/worlds`, will create a configuration file named `default.ini`. Additionally, this bash script will also install dependencies termcolor and random-geometry-points, used for generating coloured terminal output and points on the circumference of a circle with specified radius, respectively.

The configuration file `default.ini` contains information used to run this program. The default values are as follows:

| Variable | Value |
| -- | -- | -- |
| world_path | $WORLDPATH |
| max_stations | 5 |
| max_radius | 100 |
| spawn_barrier | 10 |
| min_station_distance | 5 |
| waypoints_file | waypoints.txt |

### Usage ###

`spawn.py [-h] [-n NUMBER] [-r RADIUS] [-d DISTANCE] [-b BARRIER] [-w WAYPOINTS] [-s STATIC] [-m] [-v] [-c] lat long world`

**Positional Arguments:**

| Argument | Value |
| -- | -- |
| lat | Latitude of the drone |
| long | Longitude of the drone |
| world | Name of world file |

**Optional Arguments:**

| Argument | Value |
| -- | -- |
| -h, --help | show this help message and exit |
| -n NUMBER, --number NUMBER | Number of charging stations to spawn |
| -r RADIUS, --radius RADIUS | Radius to spawn within (in metres) |
| -d DISTANCE, --distance DISTANCE | Set the minimum distance between stations (in metres) |
| -b BARRIER, --barrier BARRIER | Minimum distance between drone and stations (in metres) |
| -w WAYPOINTS, --waypoints WAYPOINTS | File containing waypoints to generate |
| -s STATIC, --static STATIC | File containing charging station and waypoint coordinates to spawn |
| -m, --map | Show the map for generated stations |
| -v, --verbose | Display banner and ask to confirm parameters |
| -c, --custom | Use custom path for world file |

#### Specifying Waypoint or Charging Station Coordinates ####

Using the `-w` OR `-s` flags (which are **mutually exclusive**), you can specify text-files containing only way-points and way-points and charging stations, respectively.

Using the `-w` flag: This will generate waypoints at coordinates specified in "waypoints.txt", where each line is an x-y coordinate - representing a way-point - separated by a comma, i.e. "x,y". Using this flag will still generate charging stations randomly.

Using the `-s` flag: This will generate waypoints AND charging stations according to the file you specify. The filename of the text-file must be **in the same directory** and have the following structure:

1. First line contains an integer, **n**, representing how many charging stations there are.
2. The next **n** lines are the x-y coordinates representing charging stations written as "x,y".
3. Every line after that represents a way-point and these coordinates follow the same syntax, "x,y".

Please see "example_static_file.txt" for a reference.

### How to Use it ###

If you want to generate waypoints as well as charging stations, the most basic line of code is as follows:

`python3 spawn.py 0 0 <world_file>`

If you want to specify just waypoints (using "waypoints.txt") and generate charging stations randomly, the most basic line of code is as follows:

`python3 spawn.py 0 0 <world_file> -w`

If you want to specify waypoint and charging station coordinates, the most basic line of code is as follows:

`python3 spawn.py 0 0 <world_file> -s <file name>`

**Extra Flags to Use:**

When testing, the verbose flag is helpful as it displays the parameters used to generate the charging station. A common line to use for testing is:

> ./spawn.py 0 0 <world_file> -m -v

Where the -m flag displays a scaled ascii map to roughly represent the distances between the drone and stations.

All coordinates are in metres. So the radius, distance between stations, and distance between drone and stations are all in metres. The -b flag (barrier) can be used to set a square area around the drone for which stations cannot be spawned.

If you want to use a world file that is not in the world path that was set in default.ini, use the `-c` flag. Using this flag will **not** change the save location of LOADME.world (the resulting world with generated stations and way-points).

### Testing it ###

Simply run `python3 test.py`. All Python unit-tests are written in this file.
