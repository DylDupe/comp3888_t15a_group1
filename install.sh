#!/bin/bash
echo -e "Installing everything you need. Sit tight ;)"
sleep 3

# Using the current directory as the base
SOURCE=`pwd`

cd $HOME
git clone https://github.com/PX4/Firmware.git --recursive && cd Firmware
bash ./Tools/setup/ubuntu.sh --no-nuttx
pip3 install dronekit

wget -P $SOURCE https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage
chmod +x $SOURCE/QGroundControl.AppImage

echo export MODELPATH=\'$HOME/Firmware/Tools/sitl_gazebo/models\' >> $HOME/.bashrc
echo export WORLDPATH=\'$HOME/Firmware/Tools/sitl_gazebo/worlds\' >> $HOME/.bashrc
echo export FIRMWARE=\'$HOME/Firmware\' >> $HOME/.bashrc
echo export SOURCE=\'$SOURCE\' >> $HOME/.bashrc

# For testing purposes
cp $SOURCE/worlds/fully_empty.world $HOME/Firmware/Tools/sitl_gazebo/worlds

echo -e "\nInstalling spawner dependencies..."
cd $SOURCE/StationSpawner
python3 setup.py <<< $HOME/Firmware/Tools/sitl_gazebo/worlds

cp $SOURCE/worlds/CP33_city_world/worlds/small_city.world $HOME/Firmware/Tools/sitl_gazebo/worlds

# Needed for current problems with Gazebo 9
pip3 install --upgrade protobuf
sudo apt upgrade libignition-math2
sudo apt-get install libprotobuf-dev

cd $HOME/Firmware

DONT_RUN=1 make px4_sitl gazebo

sudo apt-get install libgeographic-dev
sudo apt-get install geographiclib-tools

cd $SOURCE
pip3 install future
wget https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_ros_melodic.sh
source ubuntu_sim_ros_melodic.sh

echo 'source $HOME/Firmware/Tools/setup_gazebo.bash $HOME/Firmware $HOME/Firmware/build/px4_sitl_default > /dev/null' >> ~/.bashrc
echo 'export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$HOME/Firmware' >> ~/.bashrc
echo 'export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$HOME/Firmware/Tools/sitl_gazebo' >> ~/.bashrc

echo -e "\n********************\nTo finalise the installation, please run: source ~/.bashrc\n"
