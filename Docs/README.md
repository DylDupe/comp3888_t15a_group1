| Document Name |
| -- |
| [What's In This Repository and How to Use It](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/) |
| [Battery Simulation Experiment Results](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Battery_Simulation/) |
| [Drone Simulation](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Drone_Simulation/) |
| [Generating Waypoints and Charging Stations](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/StationSpawner/) |
| [World Rendering and Object Creation](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/worlds/) |
| [Weather Simulation](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Weather_Simulation/) |
| [Controlling the Drone](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Missionscript/) |
