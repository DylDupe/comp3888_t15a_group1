#!/bin/bash
echo -e "Installing everything you need. Sit tight ;)"
sleep 3

# Using the current directory as the base
SOURCE=`pwd`

# mkdir $HOME/CP34 && cd $HOME/CP34
# git clone https://DylDupe@bitbucket.org/DylDupe/comp3888_t15a_group1.git source
cd $HOME
git clone https://github.com/PX4/Firmware.git --recursive && cd Firmware
bash ./Tools/setup/ubuntu.sh --no-nuttx
pip3 install dronekit

# echo -e "\n**********************************************\n* Do you want to install QGroundControl? Y/n *\n**********************************************\n"
# read input
# if [[ "$input" =~ y|Y|Yes|yes ]] || [ -z "$input" ]; then
  # Installing QGroundControl
wget -P $SOURCE https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage
chmod +x $SOURCE/QGroundControl.AppImage
# fi

echo export MODELPATH=\'$HOME/Firmware/Tools/sitl_gazebo/models\' >> $HOME/.bashrc
echo export WORLDPATH=\'$HOME/Firmware/Tools/sitl_gazebo/worlds\' >> $HOME/.bashrc
echo export FIRMWARE=\'$HOME/Firmware\' >> $HOME/.bashrc
echo export SOURCE=\'$SOURCE\' >> $HOME/.bashrc

# OLD ENVIRONMENT VARS
# echo export WORLDPATH=\'$HOME/CP34/Firmware/Tools/sitl_gazebo/worlds\' >> $HOME/.bashrc
# echo export FIRMWARE=\'$HOME/CP34/Firmware\' >> $HOME/.bashrc
# echo export SOURCE=\'$HOME/CP34/source\' >> $HOME/.bashrc
# source $HOME/.bashrc

# For testing purposes
cp $SOURCE/worlds/fully_empty.world $HOME/Firmware/Tools/sitl_gazebo/worlds

echo -e "\nInstalling spawner dependencies..."
cd $SOURCE/StationSpawner
python3 setup.py <<< $HOME/Firmware/Tools/sitl_gazebo/worlds

cp $SOURCE/worlds/CP33_city_world/worlds/small_city.world $HOME/Firmware/Tools/sitl_gazebo/worlds

# Needed for current problems with Gazebo 9
pip3 install --upgrade protobuf
sudo apt upgrade libignition-math2
sudo apt-get install libprotobuf-dev

cd $HOME/Firmware
make clean

#sudo apt install python-rosdep2  # this line is fine
#sudo apt install python3-catkin-pkg # this line is fine
#rosdep update
#sudo rosdep init

#mkdir -p ~/catkin_ws/src && cd ~/catkin_ws/src
#sudo apt install catkin # new
#catkin_init_workspace
#cd .. && catkin_make
#source devel/setup.bash

# bash: line 32: hash: geographiclib-get-geoids: not found
# bash: line 36: hash: geographiclib-datasets-download: not found
# OS not supported! Check GeographicLib page for supported OS and lib versions.
# catkin: command not found

DONT_RUN=1 make px4_sitl gazebo

sudo apt-get install libgeographic-dev
sudo apt-get install geographiclib-tools

cd $SOURCE
# add pip3 install future
pip3 install future
wget https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_ros_melodic.sh
if [ -f ubuntu_sim_ros_melodic.sh ]; then
  #chmod u+x ubuntu_sim_ros_melodic.sh
  source ubuntu_sim_ros_melodic.sh
fi
# when ran the line above, module future no found.
# removed source line from about medolic
echo 'source $HOME/Firmware/Tools/setup_gazebo.bash $HOME/Firmware $HOME/Firmware/build/px4_sitl_default > /dev/null' >> ~/.bashrc
echo 'export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$HOME/Firmware' >> ~/.bashrc
echo 'export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$HOME/Firmware/Tools/sitl_gazebo' >> ~/.bashrc

echo -e "\n********************\nTo finalise the installation, please run: source ~/.bashrc\n"
