# Drone Control Documentation
PX4 Version - v1.11 Stable

## Battery
[See the parameters and its value range in PX4 here](https://docs.px4.io/v1.11/en/advanced_config/parameter_reference.html#battery-calibration)

By default, the battery in PX4 is disabled. It needs to be enabled by setting the parameter BAT1_CAPACITY. This can either be set directly into the PX4 terminal via typing
```
param set BAT1_CAPACITY <val>
```

or through the Mission Script

```
vehicle.parameters["BAT1_CAPACITY"] = 10000
```

or through QGroundControl manually by selecting the parameters tab and changing its value. (when connected to the drone)

Note that all drone parameters can be changed via the methods specified above.

The default minimum battery level in PX4 is 50%. This means the battery will never drain past this percentage. To change this, set the parameter SIM_BAT_MIN_PCT to 0 (via the methods above) to allow for full battery usage.

The parameter SIM_BAT_DRAIN and BAT1_CAPACITY ideally would allow for changing of the battery drain rate and the battery capacity which would alter the flight time of the drone. However, this does not work in the actual simulator and is not well documented. Our team is unsure why setting these parameters do not work.

## ROS
ROS is required for obstacle avoidance features.


### Installation
Installation of ROS is preferred to have an easier time with customising the spawn location of the drone as well as launching px4 with custom maps.


We used ROS Melodic distribution on Ubuntu 18.04.

The install.sh will do the following section for you. To install, follow the [PX4 Guide to install ROS](https://dev.px4.io/v1.10/en/setup/dev_env_linux_ubuntu.html#rosgazebo). There may be package dependencies missing during the installation, simply install them individually and then execute ubuntu_sim_ros_melodic.sh again.


The provided installation shell script should install everything for you. If not, follow the [ROS Tutorial](http://wiki.ros.org/melodic/Installation/Ubuntu) to manually install the components. 


To launch Gazebo with ROS Wrappers, you need to execute the following code as per the [PX4 Guide](https://dev.px4.io/v1.11/en/simulation/ros_interface.html#launching-gazebo-with-ros-wrappers)
```
cd <Firmware_clone>
DONT_RUN=1 make px4_sitl_default gazebo
source ~/catkin_ws/devel/setup.bash    # (optional)
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)/Tools/sitl_gazebo
roslaunch px4 posix_sitl.launch
```

(install.sh does this for you) You may also need to add these few lines to ~/.bashrc so that you don't have to execute it everytime.
```
source ${FIRMWARE}/Tools/setup_gazebo.bash ${FIRMWARE} ${FIRMWARE}/build/px4_sitl_default > /dev/null
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:${FIRMWARE}
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:${FIRMWARE}/Tools/sitl_gazebo
```
$FIRMWARE is the path to the Firmware directory, assuming you have used the provided install script. Otherwise just replace ${FIRMWARE} with the path to the Firmware folder

If you see an error that looks like this
```
RLException: [city.launch] is neither a launch file in package [px4] nor is [px4] a launch file name
The traceback for the exception was written to the log file
```
This means you have not done the above or have incorrectly added the lines to your .bashrc file

### Usage
Custom ROS launch files can be created by copying the format of the launch files in the PX4 Firmware at Firmware/launch/, where the variables such as maps, starting location of drones, number of drones etc. can be manually configured. You need the ROS wrapper above for this. Execute the launch file with
```
roslaunch px4 <filename>.launch
```


This will launch Gazebo with PX4 in the same manner as make px4_sitl gazebo commands, but with more customisable options.

## Variable Drone Spawning Location
The spawning location of the drone can be varied through changing the argument passed to the roslaunch command (or through manually modifying the launch file), and example is as follows:
```
roslaunch px4 posix_sitl_mavros.launch x:=<xPos> y:=<yPos> world:=<absolute path to world file>
```

This allows the drone to be spawned at [xPos, yPos] in the world file specified. Note that zPos can also be specified in a similar format

## Multi-Drone
Ensure you have at least PX4 v1.11 for multi-drone simulation.


Follow [PX4 Multi-Vehicle Simulation with Gazebo](https://dev.px4.io/v1.11/en/simulation/multi_vehicle_simulation_gazebo.html)


The guide provides two options, one with ROS and one without ROS. As specified before, utilising ROS will allow for positioning the starting location of each of the drones and may be more preferrable depending on your needs.


## Obstacle Avoidance - Incomplete
Follow the [PX4-Avoidance Guide](https://github.com/PX4/PX4-Avoidance) to set everything up.

Follow the section on Local Planner to launch the Obstacle Avoidance. We used the following command
```
roslaunch local_planner local_planner_stereo.launch
```

But if you want to see the drone in Gazebo as well, launch px4 in another terminal. Beware this will spawn another drone.
```
roslaunch local_planner local_planner_stereo.launch
(another terminal)
make px4_sitl gazebo
```

### Our Progress - Local Planner
The local planner is the only one that is well tested and recommended for use, however it appears to be quite primitive and simply looks around the obstacle to find a viable path. If it cannot find a path, it will be stuck in a loop trying to find an opening. 

The launch files for the Local Planner are located at 
```
~/catkin_ws/src/avoidance/local_planner/launch
```

by examining the launch file used in the example (local_planner_stereo.launch), we can see that a few lines in the launch file need to be modified OR we pass in world file as arguments in order to load our own map. The maps should ideally be located at 
```
~/catkin_ws/src/avoidance/avoidance/sim/worlds
```
pass world files as argument by:
```
roslaunch local_planner local_planner_stereo.launch world_file_name:=<your world file>
```


The local planner also launches a program called RViz, which visualises the drone's vision and what obstacles it is detecting. In order to import objects in a Gazebo world file into RViz, a .yaml file needs to also be provided that describes every object in the world file. The following is an example yaml file:
```
 - type: "mesh"
   name: "pinetree1"
   frame_id: "local_origin"
   mesh_resource: "model://pine_tree/meshes/pine_tree.dae"
   position: [4.0, 7.0, 0.0]
   orientation: [0.0, 0.0, 0.0, 1.0]
   scale: [1.0, 1.0, 1.0]

 - type: "mesh"
   name: "pinetree2"
   frame_id: "local_origin"
   mesh_resource: "model://pine_tree/meshes/pine_tree.dae"
   position: [6.1, 7.6, 0.0]
   orientation: [0.0, 0.0, 0.0, 1.0]
   scale: [1.0, 1.0, 1.0]

 - type: "mesh"
   name: "pinetree3"
   frame_id: "local_origin"
   mesh_resource: "model://pine_tree/meshes/pine_tree.dae"
   position: [19.3, -14.0, 0.0]
   orientation: [0.0, 0.0, 0.0, 1.0]
   scale: [1.0, 1.0, 1.0]
```

This file is located at 
```
~/catkin_ws/src/avoidance/avoidance/sim/worlds/simple_obstacle.yaml
```

You need to provide a corresponding .yaml file to the world file in the same name, i.e. example.world, example.yaml in the avoidance/sim/worlds/ directory.


### Going Forward
More research and probing on this feature and repo is necessary to get an implementation working. We believe that tools such as [gazebo2rviz](https://github.com/andreasBihlmaier/gazebo2rviz) can be used to convert Gazebo world files into .yaml files that RViz can load.

