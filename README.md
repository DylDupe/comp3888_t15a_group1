# CP34 - Optimal Path for Drone Delivery

**Please read this in it's entirety**

## Environment Installation ##

This repository requires the use of Gazebo, PX4, and dronekit. Please run the installation script `install.sh`.

> Note: It assumes you have a fresh Ubuntu 18.04 installation.

## In this Repository ##

This repository contains:

* An installation script `install.sh` which assumes a fresh installation of Ubuntu 18.04. It installs all dependencies along with PX4 and Gazebo as well as initialises the default configuration file required for object generation within worlds.
* A testing script `build.sh` which runs all algorithm testing files.
* A python3 program `spawn.py` - within the folder [**StationSpawner**](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/StationSpawner/) - which procedurally generates charging stations and waypoints within a given Gazebo world file AND starts Gazebo, spawning the drone at the given coordinates.
* A python3 script `mission.py` - within the folder [**MissionScript**](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/MissionScript/) - which connects to and controls the drone using a given path (requires Gazebo to be running, i.e. run `spawn.py` first).
* Information pertinent to the algorithm is within the folder [**Algorithm**](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Algorithm/).
* A folder [**worlds**](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/worlds/) which contains some basic worlds which are used for testing purposes and information on world rendering and object creation.
* Gazebo experiment results modelling weather and drone battery, these are inside the README.md files of [Weather_Simulation](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Weather_Simulation/) and [Battery_Simulation](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Battery_Simulation/) respectively.

## How Does this Project Work? ##

This project relies on multiple programs in order to function the way it was intended to. As a summary:

| Step | Description |
| -- | -- |
| **1** |  We begin with a list of waypoints and charging stations, or just waypoints. These "waypoints" are the destinations, the "drop-off" points that the drone will have to visit. If you would like to generate both the waypoints and the charging stations, please see [here](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/StationSpawner/README.md) for more information on that. |
| **2** | If there is a list, i.e. you're not generating both the waypoints AND charging stations together, then it is passed to `spawn.py`, along with a world file. If there is no list, please read the spawner documentation [here](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/StationSpawner/README.md) for more information |
| **3** | This program produces a json file, `input.json`, which is then given to the algorithm's driving python program, `main.py` [here](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/Algorithm/src). The algorithm will produce another json file, `output.json`, which contains the computed path for the drone to follow |
| **4** | The file `output.json` is given to the mission planner program `mission.py` and at this point, Gazebo has opened the world containing the generated charging stations and waypoints. From here, the drone is connected to where it will follow the path set out by `output.json` |

## Running the Code ##

There are two important scripts required to run this project.

* **spawn.py** - this python file is located in the StationSpawner folder. It will generate the world as directed AND load gazebo (through [ROSLaunch](http://gazebosim.org/tutorials?tut=ros_roslaunch)) with that world, with the drone at the specified coordinates.
* **start.sh** - passes `input.json` to the algorithm which produces `output.json`. Finally, `output.json` is passed to `mission.py` which connects to the drone in Gazebo and runs the computed path.

## Using the Individual Programs ##

> This is often used ONLY for testing purposes, please use the automation programs as directed above otherwise.

The programs mentioned above aim to automate the process of generating world files, loading these worlds, and controlling drones within them. However, you may find that you want to run things individually. Before the process is described, there are a few **environment variables** that are mentioned which require explanation. Within `~/.bashrc`, a shell script that runs every time a new shell opens, there are the following lines near the end of the file:

| Line | Reason for Existence |
| -- | -- |
| export WORLDPATH='<yourpath>' | Used by `spawn.py` to determine where to load Gazebo world files |
| export FIRMWARE='<yourpath>' | Used by `spawn.py` to start Gazebo with PX4 with the specified world file through ROSLaunch |
| export SOURCE='<yourpath>' | Used by both `spawn.py` and `start.sh` to move files in filesystems unique to the user |

The process is as follows:

1. Firstly, you need to generate the charging stations and waypoints using `spawn.py`. See [here](https://bitbucket.org/DylDupe/comp3888_t15a_group1/src/master/StationSpawner/README.md) for an in-depth explanation of the program's usage.
2. Next, you need to move the output of this program, `input.json` (named as such as it is the input to the algorithm), to the scope of the algorithm. This would be `$SOURCE/Algorithm/src/data` (here, $ is used to dereference the environment variable "SOURCE", i.e. get it's value which is unique to the user).
3. From here, navigate to the algorithm program, `main.py`, at `$SOURCE/Algorithm/src` and run `python3 main.py`. This will automatically use the `input.json` file as input, and produce `output.json`, which is the optimal path. If an input file already exists, just delete it.
4. Move the output of the algorithm, `output.json` to the scope of the mission planner, at `$SOURCE/MissionsScript`. Again, if one already exists, just delete it.
5. Then run `python3 mission.py -f output.json --px4`.
