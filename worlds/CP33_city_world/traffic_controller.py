import asyncio as aio
import logging
from typing import Iterable, Tuple
import enum

class Color(enum.Enum):
    red = "red"
    yellow = "yellow"
    green = "green"

class TrafficLightController(object):
    def __init__(self, address: str="127.0.0.1", port: int=23456):
        self.address = address
        self.port = port
        self.logger = logging.getLogger("TrafficLight Client")
        self.reader = None
        self.writer = None

    async def set_color(self, model_name: str, color: Color):
        self.logger.debug("setting {} to {}".format(model_name, color.value))
        self.writer.write(self._build_msg(model_name, color))
        await self.writer.drain()
        msg = await self.reader.readline()
        self.__log_if_error(msg)

    async def set_colors(self, *args: Iterable[Tuple[str, Color]]):
        for m, c in args:
            await self.set_color(m, c)

    @staticmethod
    def _build_msg(model: str, color: Color):
        return (model + ' ' + color.value + '\n').encode('ascii')

    def __log_if_error(self, msg):
        if b'ERROR' in msg:
            self.logger.error(str(msg, encoding='ascii'))
        elif b'WARN' in msg:
            self.logger.warning(str(msg, encoding='ascii'))

    async def connect_to_gazebo(self):
        while True:
            try:
                self.reader, self.writer = await aio.open_connection(self.address, self.port)
                self.logger.info("connected")
                break
            except ConnectionRefusedError:
                self.logger.error("Connection refused, will try again later.")
                await aio.sleep(1)

    async def close(self):
        self.writer.close()
        await self.writer.wait_closed()
