import asyncio as aio
from traffic_controller import Color, TrafficLightController

async def loop(ctl: TrafficLightController):
    while True:
        r = Color.red
        g = Color.green
        y = Color.yellow
        await ctl.set_colors(
            ("stop_light_post_170",r), ("stop_light_post_174",r),
            ("stop_light_post_178",r), ("stop_light_post_172",r),
            ("stop_light_post_176",r), ("stop_light_post_173",r),
        )
        await aio.sleep(1)
        await ctl.set_colors(
            ("stop_light_post_177",g), ("stop_light_post_180",g),
            ("stop_light_post_171",g), ("stop_light_post_175",g),
            ("stop_light_post_181",g), ("stop_light_post_179",g),
            ("stop_light_post_182",g)
        )
        await aio.sleep(7)
        await ctl.set_colors(
            ("stop_light_post_177",y), ("stop_light_post_180",y),
            ("stop_light_post_171",y), ("stop_light_post_175",y),
            ("stop_light_post_181",y), ("stop_light_post_179",y),
            ("stop_light_post_182",y),
        )
        await aio.sleep(2)
        await ctl.set_colors(
            ("stop_light_post_177",r), ("stop_light_post_180",r),
            ("stop_light_post_171",r), ("stop_light_post_175",r),
            ("stop_light_post_181",r), ("stop_light_post_179",r),
            ("stop_light_post_182",r),
        )
        await aio.sleep(1)
        await ctl.set_colors(
            ("stop_light_post_170",g), ("stop_light_post_174",g),
            ("stop_light_post_178",g), ("stop_light_post_172",g),
            ("stop_light_post_176",g), ("stop_light_post_173",g),
        )
        await aio.sleep(12)
        await ctl.set_colors(
            ("stop_light_post_170",y), ("stop_light_post_174",y),
            ("stop_light_post_178",y), ("stop_light_post_172",y),
            ("stop_light_post_176",y), ("stop_light_post_173",y),
        )
        await aio.sleep(2)


async def start():
    try:
        ctl = TrafficLightController("127.0.0.1", 23456)
        await ctl.connect_to_gazebo()

        await loop(ctl)

        await ctl.close()
    except Exception:
        ctl.logger.critical("Uncaught Exception", exc_info=True)
        raise

if __name__ == "__main__":
    aio.get_event_loop().run_until_complete(start())
