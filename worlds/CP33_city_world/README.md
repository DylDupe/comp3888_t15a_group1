## 1. Setup ##

### 1.1 Build the Traffic Light Controller Plugin ###

```{bash}
# Assume your are now in the root of source tree.
cd plugin
mkdir build
cd build
cmake ..
make
```

### 1.2 Environments ###
```{bash}

export GAZEBO_PLUGIN_PATH=/path/to/plugin/build/directory:${GAZEBO_PLUGIN_PATH}
export GAZEBO_MODEL_PATH=/path/to/model:${GAZEBO_MODEL_PATH}

# Optional: Prevent gazebo loading missing model from internet
# export GAZEBO_MODEL_DATABASE_URI=""

```

## 2. Traffic light plugin protocol

### 2.1 Protocal

This plugin is using a text protocol.
User may use commands to control traffic lights.
Each command must be separated by a newline character.
A command is a traffic light model name in gazebo followed by the color it required to be changed.
The model name and color in one command should separate by a single space character.


The formal definition of the protocol as follow.

```
NEWLINE = "\n"
SPACE = " "
identifier = {any gazebo valid model name which should not containing space or newline}
color = red | yellow | green
COMMAND = identifier SPACE color NEWLINE
```

After each command is handled by server, the server will return an "OK\n".

### 2.2 Known issue

- There is no error handling right now, to be specific User may not know if a command was executed successfully.
- The server listening address and port is hardcoded to 127.0.0.1:23456.
- Only "stop_light_post'" model from gazebo official repository supported.