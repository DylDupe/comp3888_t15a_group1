#ifndef TrafficLightController_HH
#define TrafficLightController_HH  1

#include <thread>
#include <gazebo/common/Plugin.hh>
#include <gazebo/transport/transport.hh>

namespace gazebo
{
    class TrafficLightController : public WorldPlugin
    {
        public: TrafficLightController(void);

        public: ~TrafficLightController(void);

        public: void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);

        public: void setColor(std::string model, const char *color);
        /// \brief Node used to establish communication with gzserver.
        private: transport::NodePtr node;

        /// \brief Publisher of factory messages.
        private: transport::PublisherPtr visPub;
    };

} // namespace gazebe

#endif /* TrafficLightController_HH */
