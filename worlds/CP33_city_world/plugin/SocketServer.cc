#include "SocketServer.hh"
#include <string.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <errno.h>
#include <error.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <byteswap.h>

const char *listen_ip4_addr = "127.0.0.1";
const uint16_t listen_port = 23456;

SocketServer::SocketServer(gazebo::TrafficLightController *p)
{
        this->p = p;

        if (-1 == (this->fd = socket(AF_INET, SOCK_STREAM, 0)))
                goto err;
        int optval;
        optval = 1;
        setsockopt(this->fd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int));

        struct sockaddr_in addr;
        addr.sin_family = AF_INET;
        inet_pton(AF_INET, listen_ip4_addr, &addr.sin_addr.s_addr);
        addr.sin_port = bswap_16(listen_port);

        if (-1 == bind(this->fd, (struct sockaddr *)&addr, sizeof(addr)))
                goto err;

        listen(this->fd, 4096);
        return;
err:
        error(0, errno, "unable to open socket at %s:%d\n", listen_ip4_addr, listen_port);
        this->fd = -1;
}

const int BUFFERSIZE = 4096;
class ConnectionReader
{
public: 
        ConnectionReader(int fd);
        ~ConnectionReader(void);
        char *readline(void);

private:
        bool read_more(void);
        int fd;
        bool connection_closed = false;
        bool need_more = true;
        char *base;
        char *buffer;
        size_t buffer_capacity;
        size_t buffer_size;
};

ConnectionReader::ConnectionReader(int fd)
{

        if (-1 == fcntl(fd, F_SETFL, O_NONBLOCK))
                error(0, errno, "cannot set flag");
        this->fd = fd;
        this->buffer = (char *)calloc(sizeof(char), BUFFERSIZE + 1);
        this->base = buffer;
        this->buffer_capacity = BUFFERSIZE;
        this->buffer_size = 0;
}

ConnectionReader::~ConnectionReader(void)
{
        free(this->buffer);
}

bool ConnectionReader::read_more(void)
{        
        struct pollfd fds[1];
        const nfds_t nfds = 1;
        fds[0].fd = this->fd;
        fds[0].events = POLL_IN;

again:
        int rv = poll(fds, nfds, 3000 /* ms */);
        if (rv == -1)
                return false;
        else if (rv == 0)      /* timeout */
                goto again;

        memmove(this->buffer, this->base, this->buffer_size);
        this->base = this->buffer;

        for (;;) {
                char *base = this->buffer + this->buffer_size;
                size_t rest_size = this->buffer_capacity - this->buffer_size;

                int got = recv(this->fd, base, rest_size, MSG_NOSIGNAL);
                if (got == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
                        this->buffer[this->buffer_size] = 0;
                        return true;
                }
                if (got == 0 || got == -1) {
                        this->connection_closed = true;
                        return false;
                }
                this->buffer_size += got;
                if (got == rest_size) {
                        this->buffer = (char *)realloc(this->buffer, (this->buffer_capacity <<= 1) + 1);
                        this->buffer[this->buffer_capacity+1] = 0;
                        continue;
                }
        }
}

char *ConnectionReader::readline(void)
{
again:
        char *line = strtok(this->base, "\n");
        if (!line) {
                if (!this->read_more())
                        return NULL;
                goto again;
        }

        size_t size = strlen(line) + 1;
        this->base += size;
        this->buffer_size -= size;

        return line;
}

void SocketServer::handle(int conn)
{
        ConnectionReader reader(conn);
        for (char *line = reader.readline(); line; line = reader.readline()) {
                char *model = strtok(line, " ");
                char *color = strtok(NULL, " ");

                if (model && color) {
                        this->p->setColor(model, color);
                        send(conn, "OK\n", 3, MSG_NOSIGNAL);
                } else {
                        const char *msg = "Error: Invalid command\n";
                        send(conn, msg, strlen(msg), MSG_NOSIGNAL);
                }
                             
        }
        return;
}

void SocketServer::start(void)
{
        for(;;) {
                int conn = accept(this->fd, NULL, NULL);
                this->handle(conn);
                close(conn);
        }
}
