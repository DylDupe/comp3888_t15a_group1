#include "TrafficLightController.hh"
#include <stdio.h>
#include <unistd.h>
#include "SocketServer.hh"

using namespace gazebo;

GZ_REGISTER_WORLD_PLUGIN(TrafficLightController);

#include <pthread.h>

static void start_thread(TrafficLightController *controller)
{
    pthread_setname_np(pthread_self(), "TrafficLightCtl");
    SocketServer s(controller);
    s.start();
}

TrafficLightController::TrafficLightController(void) {}
TrafficLightController::~TrafficLightController(void) {}

void TrafficLightController::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->node = gazebo::transport::NodePtr(new gazebo::transport::Node());
    this->node->Init();
    this->visPub = this->node->Advertise<gazebo::msgs::Visual>("/gazebo/default/visual");

    /* for enabling video streaming on start up. We did not make a seperated plugin for this function.
    if you do not have a model supporting video streaming, do NOT uncomment, it will hang gazebo on 
    start up.
    PX4's typhoon_h480 supports video streaming.
    */
    /*
    auto videoPub = this->node->Advertise<msgs::Int>("~/video_stream");
    videoPub->WaitForConnection();
    msgs::Int request;
    request.set_data(1);
    videoPub->Publish(request);
    */

    std::thread t(start_thread, this);
    t.detach();
}

void TrafficLightController::setColor(std::string model, const char *color)
{  
    auto red = std::make_pair(gazebo::common::Color::Red, "red");
    auto yellow = std::make_pair(gazebo::common::Color::Yellow, "yellow");
    auto green = std::make_pair(gazebo::common::Color::Green, "green");
    auto black = gazebo::common::Color::Black;

    for (auto c : {red, yellow, green}) {
        for (auto l : {"right_light", "center_light"})
        {
            gazebo::msgs::Visual msg;
            msg.set_type(gazebo::msgs::Visual::VISUAL);

            msg.set_parent_name(model + "::" + l + "::link");
            msg.set_name(model + "::"+ l + "::link::" + c.second);

            auto matMsg = msg.mutable_material();
            if (0 == strcmp(c.second, color)){
                gazebo::msgs::Set(matMsg->mutable_emissive(), c.first);
                gazebo::msgs::Set(matMsg->mutable_ambient(), c.first);
            } else {
                gazebo::msgs::Set(matMsg->mutable_emissive(), black);
                gazebo::msgs::Set(matMsg->mutable_ambient(), black);
            }

            this->visPub->Publish(msg);
        }
    }
}

 