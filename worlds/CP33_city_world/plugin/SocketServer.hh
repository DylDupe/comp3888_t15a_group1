#ifndef SOCKETSERVER_HH
#define SOCKETSERVER_HH  1

#include "TrafficLightController.hh"

class SocketServer
{
public: SocketServer(gazebo::TrafficLightController *p);
public: void start(void);

protected: void handle(int conn);

private: int fd;
private: gazebo::TrafficLightController *p;
};

#endif /* SOCKETSERVER_HH */
