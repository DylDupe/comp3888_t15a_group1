# Creating objects in Gazebo #

## Adding Objects ##
By following this tutorial on the offical website of Gazebo. (http://gazebosim.org/tutorials?tut=build_world#AddingObjects)

You will be able to create an object in the Gazebo world.

## Editing Objects ##
And following this tutorial will help you to edit the model you created from the tutorial above. (http://gazebosim.org/tutorials?tut=guided_b3)

## Color and texture of Objects ##
This tutorial will help you to change the color and the texture of the objects you created from the tutorials above. (http://gazebosim.org/tutorials?tut=color_model)

## Please read the following ##
The world files in this folder are **LOADED** worlds. This means that all of the __include__ tags, inside **UNLOADED** world files, are expanded into their respective xml code. You may find these worlds useful.

To explain what this means further, please take a look at the following example:

Below is the image of an **UNLOADED** world file which can be found in the default Gazebo worlds folder. If you have run the installation script, you can type `echo $WORLDPATH` to see where this is.

![An Unloaded World File](Images/unloaded_world.PNG "UNLOADED world")

You will see on lines 5 to 7, there is an __include__ tag which imports the sun model. The code that this is expanded to can be found in the **sun folder** in the file __model.sdf__ in your **MODELPATH** environment variable (run `echo $MODELPATH` to see where this is).

Here is the file sun model file, **model.sdf**:

![Sun model.sdf](Images/sun_model_sdf.PNG "model.sdf")

Below is a snipper of the resulting world file:

![Snippet of Loaded World File](Images/loaded_world.PNG "LOADED world")

If you navigate to your firmware folder, `cd $FIRMWARE`, and run `make px4_sitl gazebo`, the flightstack PX4 will expand these __include__ tags for you.

This information is important as it will help you better understand how world rendering works.
