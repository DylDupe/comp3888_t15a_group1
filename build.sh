#!/bin/bash

# put everything you want Jenkins to execute here
# exit with code 0 for successful build
# exit with anything else for a failure

# We must cd into the test input directory in order to correctly use command line arguments for main.py
cd Algorithm/test/test_input

output_path=../test_output
expected_output_path=../expect_output
program_path=../../src/main.py

for test_number in {1..16}
do
  python3 $program_path input$test_number.json $output_path/output$test_number.json >/dev/null
  r=$(diff $output_path/output"$test_number".json $expected_output_path/expected"$test_number".json | wc -l)

  if [ $r -gt 0 ]; then
    diff $output_path/output"$test_number".json $expected_output_path/expected"$test_number".json
    echo Test $test_number failed!
  fi
done

echo Passed!
exit 0
