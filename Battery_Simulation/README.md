# Modelling the Battery
> Using Ardupilot and QGroundPilot to set waypoints and altitude.
> Battery consumption is based on Ardupilot Iris Quad-copter battery simulation.
> Using Gazebo to find drone's pose, x, y, z axis.

**Flight Speed:** 5m/s

| Climb Rate |
| -- |
| Ascend  2.4m/s |
| Descend 1.5m/s |

| Height | Ascend Battery Cost | Descend Battery Cost | Total Battery Cost |
| -- | -- | -- | -- |
|  10m |  3% |  5% |  8% |
|  20m |  4% |  8% | 12% |
|  30m |  5% |  9% | 14% |
|  40m |  6% | 11% | 17% |
|  50m |  7% | 13% | 20% |
|  60m |  8% | 14% | 22% |
|  70m |  9% | 16% | 25% |
|  80m | 10% | 18% | 28% |
|  90m | 11% | 19% | 30% |
| 100m | 12% | 21% | 33% |
| 110m | 13% | 23% | 26% |
| 120m | 14% | 24% | 28% |

** Average Ascending m/battery percentage: ** 7.08m/battery percentage

** Average Descending m/battery percentage: ** 3.97m/battery percentage

** Average Total Battery Cost of ascending and descending m/battery percentage: ** 2.55m/battery percentage

| Altitude | Start Coordinate |	End Coordinate | Distance |
| -- | -- | -- | -- |
| 10m |	(0,0,0)	| (1855.93, 201.81, 9.22)    | 1945m |
| 20m |	(0,0,0)	| (1928.74, 185.87, 19.04)   | 1933m |		
| 30m |	(0,0,0)	| (1908.32, 57.01, 29.52)    | 1908m |					
| 40m |	(0,0,0)	| (1584.86, -1019.49, 37.98) | 1880m |					
| 40m |	(0,0,0)	| (1893.13, 59.09, 37.99)    | 1890m |			
| 50m |	(0,0,0)	| (1863.14, 182.67, 47.25)   | 1868m |				
| 60m |	(0,0,0)	| (1846.16, 114.93, 56.57)   | 1847m |					
| 70m | (0,0,0)	| (1862.02, 55.76, 66.44)    | 1824m |

** Average m/battery percentage: ** 20.02m/battery percentage
