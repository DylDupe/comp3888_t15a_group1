import sys
import json
c = 0
i = 0
f = open("output/"+sys.argv[1])
ff = open("expected/"+sys.argv[2])
data = json.load(f)
expected_data = json.load(ff)
if len(data["path"]) != len(expected_data["path"]):
    c = 1
else:
    while i < len(data["path"]):
        if data["path"][i] != expected_data["path"][i]:
            c = 1
            f.close()
            ff.close()
            exit(c)
        else:
            i += 1


f.close()
ff.close()
exit(c)