# Test Design

** This file record the specific information of each test. **

| Test No. | Description |
| -- | -- |
| 1. | Simple rectangle, enough battery, charge points not needed |
| 2 | Simple rectangle, not enough battery, charge points needed |
| 3 | One of the destination is out of reach, cannot form a path |
| 4 | Destination, charge points in a straight line 1 |
| 5 | Destination, charge points in a straight line 2 |
| 6 | Negative coordniates, Destination, charge points in a straight 1 |
| 7 | Negative coordniates, Destination, charge points in a straight 2 |
| 8 | No charge point, can charge at starting point, straight line. |
| 9 | No charge point, can charge at starting point, rectangle. |
| 10 | coordniates(x,y,z) are not 0. 3 dimentions coordnaites 1 |
| 11 | coordniates(x,y,z) are not 0. 3 dimentions coordnaites 2 |
| 12 | Weather, aviod bad weather area 1 |
| 13 | Weather, avoid bad weather area 2 |
| 14 | Same map as test13, no bad weather |
| 15 | No charge point, need to recharge at starting point 1 |
| 16 | No charge point, need to recharge at starting point 2 |

