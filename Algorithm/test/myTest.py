
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../src")

from lib.node import Node
from lib.path import Path
from lib.parser import *
import json
from lib.node import Node
from main import * 

import unittest

'''
input_obj  = Parser()
input_obj.change_input("./test_input/test_in_1.json")
my_input=input_obj.read_input()

GRAPH = create_graph(my_input.start, my_input.end, my_input.stations,my_input.reserve_battery,my_input.start_battery,my_input.max_battery)

dijkstra(GRAPH)

print("Source coordinate: {}".format(GRAPH[0].get_cord()))
print("Destination coordinate: {}".format(GRAPH[len(GRAPH) - 1].get_cord()))
print("The optimal path in reverse order is {}".format(track_path(GRAPH)))
print("The shortest distance is {}".format(GRAPH[len(GRAPH) - 1].get_min_distance()))
'''

def my_program(file_path):
    input_obj  = Parser()
    input_obj.change_input(file_path)
    my_input=input_obj.read_input()
    GRAPH = create_graph(my_input.start, my_input.end, my_input.stations,my_input.reserve_battery,my_input.start_battery,my_input.max_battery)
    dijkstra(GRAPH)

    return GRAPH[0].get_cord(), GRAPH[len(GRAPH) - 1].get_cord(), track_path(GRAPH), GRAPH[len(GRAPH) - 1].get_min_distance()


class testPath(unittest.TestCase):
    def test_1(self):
        start,end,path,distance = my_program("./test_input/test_in_1.json")
        
        self.assertEqual(start, [1.1,2,3.4])
        self.assertEqual(end, [30.2, 11.1, 15.7])
        self.assertEqual(path,[[30.2, 11.1, 15.7], [3, 3, 3], [1.1, 2, 3.4]])
        self.assertEqual(distance,33.27)
    
    def test_2(self):
        start,end,path,distance = my_program("./test_input/test_in_2.json")
        
        self.assertEqual(start, [1.1,2,3.4])
        self.assertEqual(end, [70.2, 11.1, 15.7])
        self.assertEqual(path,[[70.2, 11.1, 15.7]])
        self.assertEqual(distance,sys.maxsize)

    def test_3(self):
        start,end,path,distance = my_program("./test_input/test_in_3.json")
        
        self.assertEqual(start, [2.7,4.2,3.4])
        self.assertEqual(end, [68.2, 11.1, 15.7])
        self.assertEqual(path,[[68.2, 11.1, 15.7], [41, 42, 43], [12, 13, 14], [2.7, 4.2, 3.4]])
        self.assertEqual(distance,116.25)

    def test_4(self):
        start,end,path,distance = my_program("./test_input/test_in_4.json")
        
        self.assertEqual(start, [2.7,4.2,3.4])
        self.assertEqual(end, [58.2, 21.1, 15.7])
        self.assertEqual(path,[[58.2, 21.1, 15.7], [10, 9, 11], [2.7, 4.2, 3.4]])
        self.assertEqual(distance,61.5)
    
    def test_5(self):
        start,end,path,distance = my_program("./test_input/test_in_5.json")
        
        self.assertEqual(start, [-2.7,-4.2,-3.4])
        self.assertEqual(end, [28.2, 21.1, 15.7])
        self.assertEqual(path,[[28.2, 21.1, 15.7], [-2.7, -4.2, -3.4]])
        self.assertEqual(distance,44.27)

    def test_6(self):
        start,end,path,distance = my_program("./test_input/test_in_6.json")
        
        self.assertEqual(start, [-2.7,-4.2,-3.4])
        self.assertEqual(end, [0, 0, 0])
        self.assertEqual(path,[[0, 0, 0], [-2.7,-4.2,-3.4]])
        self.assertEqual(distance,6.04)

    def test_7(self):
        start,end,path,distance = my_program("./test_input/test_in_7.json")
        
        self.assertEqual(start, [-2.7,-4.2,-3.4])
        self.assertEqual(end, [100, 99, 98.7])
        self.assertEqual(path,[[100, 99, 98.7]])
        self.assertEqual(distance,sys.maxsize)

    def test_8(self):
        start,end,path,distance = my_program("./test_input/test_in_8.json")
        
        self.assertEqual(start, [0,0,0])
        self.assertEqual(end, [100, 99, 98.7])
        self.assertEqual(path,[[100, 99, 98.7]])
        self.assertEqual(distance,sys.maxsize)

    def test_9(self):
        start,end,path,distance = my_program("./test_input/test_in_9.json")
        
        self.assertEqual(start, [0,0,0])
        self.assertEqual(end, [100, 99, 98.7])
        self.assertEqual(path,[[100, 99, 98.7], [75, 75, 75], [50, 50, 50], [0, 0, 0]])
        self.assertEqual(distance,171.88)

    def test_10(self):
        start,end,path,distance = my_program("./test_input/test_in_10.json")
        
        self.assertEqual(start, [0,0,0])
        self.assertEqual(end, [1000, 99, 98.7])
        self.assertEqual(path,[[1000, 99, 98.7]])
        self.assertEqual(distance,sys.maxsize)
if __name__ == '__main__':
   unittest.main()

