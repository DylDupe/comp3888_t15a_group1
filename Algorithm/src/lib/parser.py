import sys, json


class Input:

    def __init__(self, 
                 max_battery,
                 reserve_battery,
                 start,
                 end,
                 stations,
                 weather):
        self.max_battery = max_battery
        self.reserve_battery = reserve_battery
        self.start = start
        self.end = end
        self.stations = stations
        self.weather = weather 


class Parser:

    def __init__(self,
                 input_file="data/input.json",
                 output_file="data/output.json"):
        self.input_file = input_file
        self.output_file = output_file

    #change input file for testing purpose 
    def change_input(self,input_file):
        self.input_file = input_file
    def change_output(self,output_file):
        self.output_file = output_file   

    def read_input(self):

        try:
            with open(self.input_file, 'r') as input_file:
                input_json = dict(json.load(input_file))

        except Exception as e:
            print("\n\nread input file error\n{}".format(str(e)), file=sys.stderr)
            sys.exit(1)

        max_battery = 0
        reserve_battery = 0
        start = None
        end = None
        stations = None
        weather = None 

        for key, value in input_json.items():



            if "max_battery" == key:
                max_battery = value

            elif "start" == key:
                start = value

            elif "end" == key:
                end = value

            elif "stations" == key:
                stations = value
            elif "weather" == key:
                weather = value 
        if  max_battery <= 0 \
                or start is None \
                or end is None \
                or stations is None:
            print("\n\ninput file configuration error")
            sys.exit(1)

        return Input(max_battery=max_battery,
                     reserve_battery=reserve_battery,
                     start=start,
                     end=end,
                     stations=stations,
                     weather = weather)
