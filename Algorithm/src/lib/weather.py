import math
import numpy as np
from numpy.linalg import solve
old_settings = np.seterr(all='ignore')
class Weather:

    def __init__(self, weather_cond):
        self.weather_cond = weather_cond

    def calculate_distance(self, a, b):
        r = (a[0]-b[0])**2 + (a[1]-b[1])**2 + (a[2]-b[2])**2
        distance = math.sqrt(r)
        return distance

    def calculate_surface(self, a, b, c):
        # a, b, c are the three points needed to calculate the surface equation
        # return a surface equation
        #A = np.mat([[a[0],a[1],a[2],1], [b[0],b[1],b[2],1], [c[0],c[1],c[2],1]])
        #b = np.mat([0,0,0]).T
        #x = solve(A, b)
        #return x
        p1 = np.array([a[0], a[1], a[2]])
        p2 = np.array([b[0], b[1], b[2]])
        p3 = np.array([c[0], c[1], c[2]])

        # These two vectors are in the plane
        v1 = p3 - p1
        v2 = p2 - p1

        # the cross product is a vector normal to the plane
        cp = np.cross(v1, v2)
        a, b, c = cp

        # This evaluates a * x3 + b * y3 + c * z3 which equals d
        d = np.dot(cp, p3)*(-1)
        return [a,b,c,d]

    def calculate_intersection(self, a, b, x):
        # a, b are the two points we need to calculate the line equation
        # x is the parameters a, b, c, d for a surface
        # return a point coordinate
        p1 = np.array([a[0], a[1], a[2]])
        p2 = np.array([b[0], b[1], b[2]])
        plane_normal = np.array([x[0],x[1],x[2]])
        P1D = (np.vdot(p1,plane_normal)+x[3])/np.sqrt(np.vdot(plane_normal,plane_normal))
        P1D2 = (np.vdot(p2-p1,plane_normal))/np.sqrt(np.vdot(plane_normal,plane_normal))
        if P1D*P1D2 >=0:
            n = P1D2/P1D
        else:
            n = abs(P1D/P1D2)
        p = p1 + n*(p2- p1)
        return [p[0],p[1],p[2]]

    def getLinearLength(self, start, end):
        index_list = [[0,1,2], [0,1,4], [1,3,5], [2,3,6], [0,2,4], [4,5,6]]
        p_list = []
        inter_list = []
        for w in self.weather_cond:
            for weather_name in w:
                coord = sorted(w[weather_name][0:8])
                xmin = coord[0][0]
                xmax = coord[7][0]
                ymin = coord[0][1]
                ymax = coord[7][1]
                zmin = coord[0][2]
                zmax = coord[7][2]
                if ((start[0]<xmin and end[0]<xmin) or
                    (start[0]>xmax and end[0]>xmax) or
                    (start[1]<ymin and end[1]<ymin) or
                    (start[1]>ymax and end[1]>ymax) or
                    (start[2]<zmin and end[2]<zmin) or
                    (start[2]>zmax and end[2]>zmax)):
                    continue
                else:
                    n = 0
                    while n<6:
                        x = self.calculate_surface(coord[index_list[n][0]], coord[index_list[n][1]], coord[index_list[n][2]])
                        p = self.calculate_intersection(start, end, x)
                        p_list.append(p)
                        n = n+1
                    for p in p_list:
                        if (p[0]>=xmin and p[0]<=xmax and
                            p[1]>=ymin and p[1]<=ymax and
                            p[2]>=zmin and p[2]<=zmax):
                            inter_list.append(p)
                    if len(inter_list)==0:
                        return 0.0
                    p1 = inter_list[0]
                    for i in inter_list:
                        if i!=p1:
                            p2 = i
                    #print(p1,p2)
                    ap1 = [start[0]-p1[0], start[1]-p1[1], start[2]-p1[2]]
                    ap2 = [start[0]-p2[0], start[1]-p2[1], start[2]-p2[2]]
                    bp1 = [end[0]-p1[0], end[1]-p1[1], end[2]-p1[2]]
                    bp2 = [end[0]-p2[0], end[1]-p2[1], end[2]-p2[2]]
                    a_out_cube = (ap1[0]*ap2[0]>=0) and (ap1[1]*ap2[1]>=0) and (ap1[2]*ap2[2]>=0)
                    b_out_cube = (bp1[0]*bp2[0]>=0) and (bp1[1]*bp2[1]>=0) and (bp1[2]*bp2[2]>=0)
                    if a_out_cube:
                        if b_out_cube:
                            return self.calculate_distance(p1,p2)
                        else:
                            if ap1[0]/ap2[0] > 1:
                                return self.calculate_distance(p2,end)
                            else:
                                return self.calculate_distance(p1,end)
                    else:
                        if b_out_cube:
                            if bp1[0]/bp2[0] > 1:
                                return self.calculate_distance(p2,start)
                            else:
                                return self.calculate_distance(p1,start)
                        else:
                            return self.calculate_distance(start,end)
        return 0.0



    def getAffectedLength(self, start, end):
        for w in self.weather_cond:
            for weather_name in w:
                coord = sorted(w[weather_name][0:8])
                xmin = coord[0][0]
                xmax = coord[7][0]
                ymin = coord[0][1]
                ymax = coord[7][1]
                zmin = coord[0][2]
                zmax = coord[7][2]
                if ((start[0]<xmin and end[0]<xmin) or
                    (start[0]>xmax and end[0]>xmax) or
                    (start[1]<ymin and end[1]<ymin) or
                    (start[1]>ymax and end[1]>ymax) or
                    (start[2]<zmin and end[2]<zmin) or
                    (start[2]>zmax and end[2]>zmax)):
                    continue
                else:
                    return self.getLinearLength(start,end)*w[weather_name][8]
        return 0.0
