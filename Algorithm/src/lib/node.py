import sys


class Node:
    def __init__(self, cord,node_type):
        self.cord = cord
        self.last_node = None
        self.paths = []
        self.min_distance = sys.maxsize  # minimum distance from Source to this node
        self.visited = False
        self.node_type = node_type

    def reset(self):
        self.visited=False 
        self.min_distance = sys.maxsize
        self.last_node=None

    def check_visit(self):
        return self.visited

    def set_visit(self):
        self.visited = True

    def get_paths(self):
        return self.paths

    def add_path(self, n_path):
        self.paths.append(n_path)

    def get_last(self):
        return self.last_node

    def get_cord(self):
        return self.cord

    def get_min_distance(self):
        return self.min_distance

    def set_min_distance(self, min_distance):
        self.min_distance = round(min_distance,2)

    def set_last(self, last_node):
        self.last_node = last_node
