import numpy as np


def calculate_distance(a, b):
    d1 = np.array(a)
    d2 = np.array(b)
    squared_dist = np.sum((d1 - d2) ** 2, axis=0)
    dist = np.sqrt(squared_dist)
    return round(dist, 2)


class Path:

    def __init__(self, start_node, end_node,end_type):
        self.start_node = start_node
        self.end_node = end_node
        self.distance = calculate_distance(start_node.get_cord(), end_node.get_cord())
        self.end_type = end_type #type of node, charge_point? destination?


    def get_distance(self):
        return self.distance

    def get_end_node(self):
        return self.end_node
    def get_start_node(self):
        return self.start_node

    def set_distance(self, distance):
        self.distance = distance
