from lib.weather import Weather
from lib.node import Node
from lib.path import Path
from lib.path import calculate_distance
from lib.parser import *
import json
import sys
import itertools
from itertools import groupby
import os.path


class Graph():
    def __init__(self,start,charge,end):
        self.start_point = start
        self.charge_points = charge
        self.end_points = end




def produce_output(graph, output_path,dist,my_path):
    
    #swap y, x to x,y for consistancy

    #graph.start_point.get_cord()[0] , graph.start_point.get_cord()[1] = graph.start_point.get_cord()[1] , graph.start_point.get_cord()[0]
    for i in range(len(graph.end_points)):
        graph.end_points[i].get_cord()[0], graph.end_points[i].get_cord()[1] =graph.end_points[i].get_cord()[1], graph.end_points[i].get_cord()[0]
    
    for i in range(len(graph.charge_points)):
        graph.charge_points[i].get_cord()[0], graph.charge_points[i].get_cord()[1] = graph.charge_points[i].get_cord()[1], graph.charge_points[i].get_cord()[0]


    lines = {
        "start": graph.start_point.get_cord(),
        "end": [i.get_cord() for i in graph.end_points],
        "path": my_path,
        "distance": dist
    }

    with open(output_path, "w") as output_file:
        json.dump(lines, output_file)
        output_file.write("\n")

    print("Result saved in {}".format(output_path))


def find_charge(node,reserved_battery):


    if node.node_type =="START":
        return True

    cur_paths = node.get_paths()
    min_charge_dis = sys.maxsize
    for i in range(len(cur_paths)):

        if cur_paths[i].end_type =="CHARGE" or cur_paths[i].end_type=="START":

            cur_dis = cur_paths[i].get_distance()
            if cur_dis < min_charge_dis:
                min_charge_dis=cur_dis

    if reserved_battery >= min_charge_dis:
        return True
    else:
        return False

# Begin dijkstra
# A utility function to find the vertex with
# minimum distance value, from the set of vertices
# not yet included in shortest path tree

def min_distance(node_lst,end_node,reserved_battery,charge_points,max_battery):

    for i in range(len(node_lst)):
        cur_node = node_lst[i]
        if cur_node.check_visit():  # if visited

            cur_paths = cur_node.get_paths()
           
            for j in range(len(cur_paths)):

                #exclude paths to other destination
                if cur_paths[j].end_type =="END" and cur_paths[j].get_end_node().get_cord() != end_node.get_cord():
                    #print("qidai2")
                    #print(cur_paths[j].end_type)
                    #print(cur_paths[j].get_start_node().get_cord())
                    #print(cur_paths[j].get_end_node().get_cord())
                    #print("i {}, j{}".format(i,j))
                    #print("cur {}, next{}".format(cur_node.get_cord(),cur_paths[j].get_end_node().get_cord()))
                    continue

                if i==0 and cur_paths[j].get_distance() > reserved_battery:
                    #print("qidai1")
                    #print("i {}, j{}".format(i,j))
                    #print("cur {}, next{}".format(cur_node.get_cord(),cur_paths[j].get_end_node().get_cord()))
                    continue

                cur_dis = cur_paths[j].get_distance() + cur_node.get_min_distance()
                next_node = cur_paths[j].get_end_node()

                #print("i {}, j{}".format(i,j))
                #print("cur {}, next{}".format(cur_node.get_cord(),next_node.get_cord()))

                #check it has enough battery move to next charge
                if cur_paths[j].end_type !="CHARGE":
                    checker_battery = reserved_battery-cur_paths[j].get_distance()
                    if cur_node.node_type =="CHARGE":
                        checker_battery = max_battery - cur_paths[j].get_distance()
                    

                    if not find_charge(next_node,checker_battery):

                        continue

                #print("shabi {}, {}".format(next_node.get_min_distance(),cur_dis))
                #print("bisha cur {}, next{}".format(cur_node.get_cord(),next_node.get_cord()))

                if next_node.get_min_distance() > cur_dis:
                    #print("2 start {}".format(cur_node.get_cord()))
                    #print("2 end {}".format(next_node.get_cord()))
                    next_node.set_min_distance(cur_dis)
                    next_node.set_last(cur_node)


def track_path(target_node):
    # return optimal path to target node
    #target_node = points[len(points) - 1]
    track_lst = [target_node.get_cord()]
    last_node = target_node.get_last()

    while last_node is not None:
        track_lst.append(last_node.get_cord())
        last_node = last_node.get_last()

    return track_lst


def dijkstra(start_point,end_point,charge_points,reserved_battery,max_battery):
    status = 0
    start_point.set_min_distance(0)
    start_point.set_visit()
    points = [start_point]+charge_points +[end_point]

    
    for count in range(len(points)):
        min_distance(points,end_point,reserved_battery,charge_points,max_battery)

        # get unvisited shortest path
        min_dis = sys.maxsize
        min_idx = None
        for i in range(len(points)):
            if not points[i].check_visit():
                if points[i].get_min_distance() < min_dis:
                    min_idx = i
                    min_dis = points[i].get_min_distance()

        # set new node to visited
        if not min_idx is None:
            points[min_idx].set_visit()


    result_node = points[len(points)-1]

    #testing

    if result_node.get_last() == None:
        print("{} is out of reach, cannot deliver to all destinations".format(result_node.get_cord()))
        status =1
        return None,None,status

    #testing end

    #if it go through charge point, reserve battery is reset
  #if result_node.get_last().get_cord() != start_point.get_cord():
       # result_battery= calculate_distance(result_node.get_last().get_cord(),result_node.get_cord())
    if result_node.get_last().node_type =="CHARGE" or result_node.get_last().node_type=="START":
        #print("qiama ")
        result_battery= max_battery-calculate_distance(result_node.get_last().get_cord(),result_node.get_cord())
    else:
        result_battery = reserved_battery-result_node.get_min_distance()-result_node.get_last().get_min_distance()

    return result_node, result_battery, status

def is_valid_path(cur_path, cur_battery, reserve_battery,path_type):
    # type =0  start point to end point
    # type = 1 start point to charge point
    # type = 2 charge point to charge point
    # type = 3 charge point to end point
    # return true if is a valid path, vice-versa
    if path_type ==0 or path_type==3:
        # start to end
        if cur_path.get_distance() + reserve_battery <= cur_battery:
            return True
        return False
    if path_type == 1 or path_type ==2:
        # start to charge
        if cur_path.get_distance()  <= cur_battery:
            return True
        return False

    print("Error path type")
    return False


def create_graph(start_point, end_point, stations,max_battery, weather_obj):
    weather_check = False 
    wea_obj = None
    if weather_obj != None:
        wea_obj = Weather(weather_obj)
        weather_check = True 
    
    stations.append(start_point)
    start_point = Node(start_point,'START')
    end_points= [Node(i,"END") for i in end_point]

    # Create Paths
    for i in range(len(end_points)):
        cur_path = Path(start_point,end_points[i],"END")
        if weather_check:
            update_distance = wea_obj.getAffectedLength(start_point.get_cord(),end_points[i].get_cord()) + cur_path.distance
            cur_path.distance = update_distance
        if cur_path.get_distance()<= max_battery:
            start_point.add_path(cur_path)
            sec_path = Path(end_points[i],start_point,"START")
            sec_path.distance = cur_path.get_distance()
            end_points[i].add_path(sec_path)
    


    charge_points = [Node(station,"CHARGE") for station in stations]


    for i in range(len(charge_points)):
        cur_point = charge_points[i]

        # add start to charge point if within battery range
        cur_path = Path(start_point,cur_point,"CHARGE")
        if weather_check:
            update_distance = wea_obj.getAffectedLength(start_point.get_cord(),cur_point.get_cord()) + cur_path.distance
            cur_path.distance = update_distance
        if cur_path.get_distance()<=max_battery:
            start_point.add_path(cur_path)
            sec_path = Path(cur_point,start_point,"START")
            sec_path.distance = cur_path.get_distance()
            cur_point.add_path(sec_path)

        for j in range(len(charge_points)):
            if i != j:
                # add charge to charge point if within battery range

                cur_path = Path(cur_point,charge_points[j],"CHARGE")
                if weather_check:
                    update_distance = wea_obj.getAffectedLength(cur_point.get_cord(),charge_points[j].get_cord()) + cur_path.distance
                    cur_path.distance = update_distance
                if cur_path.get_distance()<=max_battery:
                    cur_point.add_path(cur_path)


    # add charge to end path based on distance from end point to nearest charge points.
    for i in range(len(charge_points)):
        for j in range(len(end_points)):
            point_x = charge_points[i]
            point_y = end_points[j]
            path_x=Path(point_x, point_y,"END")
            
            if weather_check:
                update_distance = wea_obj.getAffectedLength(point_x.get_cord(),point_y.get_cord()) + path_x.distance
                path_x.distance = update_distance

            path_y=Path(point_y, point_x,"CHARGE")
            if weather_check:
                update_distance = wea_obj.getAffectedLength(point_y.get_cord(),point_x.get_cord()) + path_y.distance
                path_y.distance = update_distance
            if path_x.get_distance() <= max_battery:
                point_x.add_path(path_x)
                point_y.add_path(path_y)


    # add paths between end_point
    for i in range(len(end_points)):
        cur_point = end_points[i]
        for j in range(len(end_points)):
            if i!=j:
                cur_path = Path(cur_point,end_points[j],"END")
                if weather_check:
                    update_distance = wea_obj.getAffectedLength(cur_point.get_cord(),end_points[j].get_cord()) + cur_path.distance
                    cur_path.distance = update_distance

                if cur_path.get_distance()<=max_battery:
                    cur_point.add_path(cur_path)

    my_graph = Graph(start_point,charge_points,end_points)


    return my_graph

def reset_graph(graph):
    for i in range(len(graph.charge_points)):
        graph.charge_points[i].reset()

    for i in range(len(graph.end_points)):
        graph.end_points[i].reset()

    graph.start_point.reset()

def branch_and_bound(cur_battery,rest):
    max_battery = cur_battery

    # all possible arrangement of destination points
    tree_end_points =list(itertools.permutations(rest.end_points))
    #convert list of turple oto list of list
    tree_end_points = [list(ele) for ele in tree_end_points]

    cur_start_point = rest.start_point
    cur_best_path=[]
    cur_best_distance =sys.maxsize
    for i in range(len(tree_end_points)):


        cur_branch = tree_end_points[i]
        cur_branch = [rest.start_point] + cur_branch + [rest.start_point]
        sub_best_path = []
        sub_best_distance=cur_best_distance
        sub_battery = cur_battery
        check_complete=True
        sub_sum =0
        for j in range(len(cur_branch)-1):
            cur_start_point = cur_branch[j]
            cur_end_point = cur_branch[j+1]



            new_end_point,result_battery,status = dijkstra(cur_start_point,cur_end_point,rest.charge_points,sub_battery,max_battery)

            if status ==1:
                check_complete=False
                reset_graph(rest)
                break

            # get battery lost
            sub_battery = result_battery

            sub_sum+=new_end_point.get_min_distance()
            if sub_battery >0 and cur_best_distance>sub_sum:

                sub_best_distance=new_end_point.get_min_distance()
                #sub_sum += new_end_point.get_min_distance()


                sub_best_path += track_path(new_end_point)[::-1]

                #print("sub path {}".format(sub_best_path))

                #sub_battery = sub_battery -sub_best_distance

            else:
                sub_sum -=new_end_point.get_min_distance()
                #reset diakstra
                check_complete=False
                reset_graph(rest)
                break
            reset_graph(rest)
        if check_complete:

            cur_best_path=sub_best_path
            cur_best_distance = sub_sum
            reset_graph(rest)

    print("test best path")
    print(cur_best_path)
    print("test best distance")
    print(cur_best_distance)
    res = [i[0] for i in groupby(cur_best_path)]
    print(res)
    if len(res)==0:
        cur_best_distance=0
    return res, round(cur_best_distance,2)


if __name__ == "__main__":



   
    #home = os.path.join("..")
    #home = os.environ['SOURCE']
    #home = '/Users/jimmyliu/Documents/University/2020/COMP3888/3888bitbucket'
    home = os.getcwd()

    test_folder_in = os.path.join("..", "test","test_input")
    test_folder_out = os.path.join("..", "test","test_output")
    home = os.environ['SOURCE']
    # home = '/Users/jimmyliu/Documents/University/2020/COMP3888/3888bitbucket'

    input_obj  = Parser()
    if len(sys.argv)==3:
        #input_file = os.path.join(home, "Algorithm","test", "test_input", sys.argv[1])
        #output_file = os.path.join(home, "Algorithm","test", "test_output", sys.argv[2])
        input_file = os.path.join(home, sys.argv[1])
        output_file = os.path.join(home,sys.argv[2])
        input_obj.change_input(input_file)
        input_obj.change_output(output_file)
        #python3 main.py ../test/jim_input/input_3.json ../test/jim_output/outpu3.json


    my_input=input_obj.read_input()

    



    GRAPH = create_graph(my_input.start, my_input.end, my_input.stations,my_input.max_battery,my_input.weather)

  
  
  
    my_path, my_dist= branch_and_bound(my_input.max_battery,GRAPH)
    produce_output(GRAPH,input_obj.output_file,my_dist,my_path)

