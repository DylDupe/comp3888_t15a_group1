# Phase 1 Algorithm realisation 


## Update
* The program has been updated to use json file as input and output. See data/

## Introduction

This is a point to point optimal path finding algorithm based on dijkstra's algorithm. More information about this phase can be found on Google drive/Main Algorithm/COMP3888_phase 1. This document will focus on the usage of this program itself. Note that this program is currently written in Python.



## Description

There are three .py files and one txt file. 

**node.py** is a class of nodes. Starting point, destination and charging stations will be repensted as nodes. 

**path.py** is a class of paths. It indicates the path the drone  takes from node A to node B.  

**main.py**, run this file and it returns the shortest distacne from source to destination and a list of (x,y,z) coordinates represent the path. 

**coordinates.txt**, this file accepts inputs, each row shows (x,y,z) coordinate of a stop, seperated by "," 

Source coordinate goes to row 1, destination goes to to row 2, and the remaming rows are coordniates of charging stations.

e.g. coordniates.txt

1.1,2.2,3.3    ->source

11,12,13.    ->destination

5.1,5.2,5.4.     -> charge station

12,21,23. -> charge station



## How to run

1. Enter correct coordinates in __coordinates.txt__ 
2. Run **main.py** 

### Output 

1. The shortest distance from start to goal, rounded to 2 decimal numbers.
2. The shortest path represnted in a list of coordniates. 



### Note 

In this stage we haven't consider battery life and other conditions that might affect the real distance, so the shortest distance will be from start to finish in a straight line. But we can manually set the distance of a path to test if this algorihtm works. In **path.py**, **set_distance()** can manaually adjust the distance between nodes to force the algorihtm to pick other paths.





 